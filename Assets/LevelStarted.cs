using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelStarted : MonoBehaviour
{
    public UnityEvent onLevelStarted;

    private void Start() {
        Debug.Log("BBBBBBBBBBBBBB");
        onLevelStarted?.Invoke();
    }
}
