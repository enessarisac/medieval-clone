using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "SoldierData", menuName = "Medieval/SoldierData")]
public class SoldierLevelData : ScriptableObject
{
    [Header("Requirements")]
    [SerializeField] public  int StartLevel;
    [SerializeField] public  int MaxLevel;
    public GameObject[] modelPrefabs;

    
}
