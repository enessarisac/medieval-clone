using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Boss : MonoBehaviour
{
    [SerializeField] EnemyScriptable enemyScriptable;
    [SerializeField] int health;
    [SerializeField] Enemy enemy;
    
    [SerializeField] UnityEvent onBossDead;
    private void Awake() 
    {
        health = enemyScriptable.baseHealth;
        
    }
    private void Update()
    {
        health = enemyScriptable.currentHealth;

        if (health <= 0)
        {
            Dead();
        }
    }

    private void Dead()
    {

        Debug.Log("Boss has died");
        onBossDead?.Invoke();
    }

}
