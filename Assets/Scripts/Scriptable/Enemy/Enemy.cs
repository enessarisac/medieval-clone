using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
public class Enemy : MonoBehaviour
{
    [SerializeField] private EnemyScriptable enemyData;
    SoldierData soldierData;
    [SerializeField] UnityEvent onTakeDamage;
    [SerializeField] ParticleSystem particle;
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] UnityEvent onPlayerDeath;
    [SerializeField] ParticleSystem deathParticle;


    private int health;
    private void Awake()
    {
        text = GetComponentInChildren<TextMeshProUGUI>();
        this.health=enemyData.baseHealth;
        enemyData.currentHealth=this.health;   
    }
    private void Update() {
        text.text=this.health.ToString();
    }
    public void OnTakeDamage (GameObject gameObject, int damage)
    {
        soldierData=gameObject.GetComponent<SoldierData>();
        particle.Play();
        Debug.Log(soldierData.name + "Paşam");
        onTakeDamage?.Invoke();
        if(this.health<damage)
        {
            this.health -= damage;
            for(int i=0;i<damage;i++)
            {
                Debug.Log("Enemy has taken damage");
                soldierData.LevelDown(gameObject,1);
            }
        }
        else if(damage<this.health || damage == this.health)
        {
            Debug.Log("Soldier take damage");
            this.health -= damage;
            Collect.instance.SeperateSoldier(gameObject,soldierData.index+1);
            if(soldierData.name == "PlayerParent")
            {
                onPlayerDeath?.Invoke();
                Debug.Log("Invoked");
            }
            StartCoroutine(soldierData.Death(gameObject));
        }
        if (health <= 0)
        { 
            this.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
            this.GetComponent<Collider>().enabled = false;
            deathParticle.Play();
            StartCoroutine(Death());
        }
        enemyData.currentHealth=this.health;
    }
    IEnumerator Death()
    {
        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);
    }
}
