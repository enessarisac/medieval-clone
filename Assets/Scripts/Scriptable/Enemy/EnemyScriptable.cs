using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[CreateAssetMenu(fileName = "EnemyData", menuName = "Medieval/EnemyData")]
public class EnemyScriptable : ScriptableObject
{
    public int baseHealth;
    public int currentHealth;
    private void Start()
    {
        currentHealth = baseHealth;
    }
}
