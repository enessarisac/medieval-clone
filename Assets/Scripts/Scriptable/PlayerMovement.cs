using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public class PlayerMovement : MonoBehaviour
{
    public static UnityAction<bool> onPlayerMovementChanged;
    public float speed;
    Transform player;
    Animator animator;
    bool isMoving;
    float startTouch, slideTouch;
    float swipeDelta;
    public float slideSpeed = 2;
    float touchDistance;
    public float maxX, minX;

    LevelStarted levelManager;
    private void Start() 
    {
        animator=GetComponentInChildren<Animator>();
        levelManager = FindObjectOfType<LevelStarted>();
        levelManager.onLevelStarted.Invoke();
        speed=0;

    }
    private void FixedUpdate() 
    {
        Debug.Log(Time.deltaTime);
        transform.Translate(Vector3.forward *  speed * Time.deltaTime);
        if(speed>0)animator.Play("run");
        else animator.Play("idle");
      
    }
    public void PositionToZero()
    {
        player=GetComponent<Transform>();
        Vector3 position = player.position;
        position.x = 0;
        player.position = position;    
    }

}
