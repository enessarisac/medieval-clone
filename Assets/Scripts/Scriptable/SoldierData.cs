using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class SoldierData : MonoBehaviour
{
    public static SoldierData instance;
    [Header("Requirements For Scriptable")]
    [SerializeField] private SoldierLevelData soldierLevelData;
    public int level;
    int maxLevel;
    private PlayerMovement playerMovement;
    [Header("Level Meshes (Change It From Scriptable)")]
    [Tooltip("Create Scriptable From : Create/Medieval/SoldierData")]
    [SerializeField] private GameObject [] prefabs;
    public int index;
    [SerializeField] private TextMeshProUGUI lvText;
    [SerializeField] private ParticleSystem onMeshChangeParticle;

    Animator animator;
    private void Awake() 
    {
        instance = this;
    }

    private void Start() 
    {
        prefabs[0].SetActive(true);
        maxLevel = soldierLevelData.MaxLevel;
        level = soldierLevelData.StartLevel;
    }
    private void Update() 
    {
       lvText.text="LV. "+level.ToString();
    }
    public void LevelUp(GameObject gameObject)
    {
        if(level < maxLevel)
        {
            level++;
            MeshChanger(gameObject);   
        }
    
    }
    public void LevelDown(GameObject gameObject, int index)
    {
            if(level - index > 0)
            {
                level -= index;
                MeshChanger(gameObject);
            }
            else
            {
                level = 1;
                MeshChanger(gameObject);
            }                     
    }
    public void MeshChanger(GameObject gameObject)
    {
        onMeshChangeParticle.Play();
        for(int i=0;i<prefabs.Length;i++)
        { 
            Debug.Log("count"+prefabs.Length+i);
           gameObject = prefabs[i];
            if(i == level-1)
            {
                Debug.Log("Mesh Change"+i);
                gameObject.SetActive(true);
                animator = gameObject.GetComponent<Animator>();
                animator.Play("run");
                Debug.Log(gameObject.name);
            }
            else gameObject.SetActive(false);
        }
    }
    
    public IEnumerator Death(GameObject gameObject)
    {

        yield return new WaitForSeconds(0.1f);
        Debug.Log("Soldier has died"+gameObject.name);
        Destroy(gameObject);
    }
}
