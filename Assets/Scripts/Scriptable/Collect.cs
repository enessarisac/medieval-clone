using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class Collect : MonoBehaviour
{
   public static Collect instance;

   [Header("Chain Members")]
   public List<GameObject> soldiers = new List<GameObject>();

   [Header("Chain Move Variables")]
   public float movementSpeed = 0.25f;
   public float returnDelay ;
   public float offset;
   bool isMoving;
  [SerializeField] UnityEvent onChainSeperate;


   
   SoldierData soldierData;
   private void Awake() 
   {
       instance = this;
   }

    private void OnEnable() 
    {
        PlayerSwerveMovement.onPlayerMovementChanged  += OnPlayerMovementChanged;
    }

    private void OnPlayerMovementChanged(bool arg0)
    {
        isMoving = arg0;
    }

    public void AddSoldier(GameObject go)
   { 
       StackSoldier(go,soldiers.Count-1);
   }
   public void StackSoldier(GameObject soldier, int index)
   {
        Debug.Log(index);
      //  soldier.transform.parent=transform;
        Vector3 newPosition = soldiers[index].transform.position;
        newPosition.z += offset;
        soldier.transform.position = newPosition;
        soldiers.Add(soldier);
        soldier.GetComponentInChildren<Animator>().Play("run");
        StartCoroutine(MakeChainBiggerSize());
        
        for(int i=index;i<soldiers.Count;i++)
        {
            soldierData = soldiers[i].GetComponent<SoldierData>();
        }
        soldierData.index = index;     
   }
   
     IEnumerator MakeChainBiggerSize()
   {
       for(int i = soldiers.Count-1; i>=1; i--)
       {
           int soldiersIndex = i;
           Vector3 scale = Vector3.zero;
           scale=new Vector3(1,1,1);
           Vector3 doScale = scale * 1.2f;
           soldiers[soldiersIndex].transform.DOScale(doScale, 0.2f).onComplete = () =>
           soldiers[soldiersIndex].transform.DOScale(scale, 0.2f); 
           if(i==1)
           {
               break;
           }
           yield return new WaitForSeconds(0.02f);
       }
       
   }
   IEnumerator BackToNormalSize(GameObject gameObject)
   {
        yield return new WaitForSeconds(0.1f);
        gameObject.transform.DOScale(new Vector3(1,1,1),0.5f);
   }
   public  void SeperateSoldier(GameObject soldier, int index)
   {  
       onChainSeperate?.Invoke();
       int soldiersCount = soldiers.Count;
       soldier.GetComponentInChildren<Animator>().Play("idle");
       Debug.Log("Şuanki index: " + index);
        for(int i=soldiersCount-1; i>index; i--)
        { 
            int seperatePower = Random.Range(1000,2000);
            GameObject gameObject = soldiers[i];

            Vector3 randomPosition = new Vector3(Random.Range(-8,8),
            gameObject.transform.position.y,
            Random.Range(gameObject.transform.position.z+10,gameObject.transform.position.z+20));

            gameObject.transform.parent=null;

            Destroy(gameObject.GetComponent<Collision>());
            gameObject.GetComponent<SoldierData>().index = 0;

            gameObject.tag = "Soldier";
            gameObject.name="Soldier";
            Rigidbody rb =gameObject.GetComponent<Rigidbody>();
            rb.AddForce(Vector3.forward*seperatePower);
            if(gameObject.GetComponent<SoldierData>().level>1)
            {
                gameObject.GetComponent<SoldierData>().LevelDown(gameObject,gameObject.GetComponent<SoldierData>().level);
            }
            else
            {
                gameObject.GetComponent<SoldierData>().level=1;
            }
            StartCoroutine(WaitForCollider(gameObject));
            soldiers.Remove(gameObject);                                   
        }
   }
   IEnumerator WaitForCollider(GameObject gameObject)
   {
       yield return new WaitForSeconds(1);
       gameObject.GetComponent<BoxCollider>().isTrigger = true;
       gameObject.GetComponent<Rigidbody>().velocity=Vector3.zero;  
   }

    private void ChainMovement()
    {
        for(int i = 1; i < soldiers.Count; i++)
        {
            Vector3 position = soldiers[i].transform.position;
            position.x = soldiers[i - 1].transform.position.x;
            var dif = position.x - soldiers[i].transform.position.x;
            soldiers[i].transform.position += Vector3.right * dif*Time.deltaTime*movementSpeed; 
        }
    }
    void Update()
    {   
        for(int i=0;i<soldiers.Count;i++)
       {
           if(soldiers[i]==null)
           {
               soldiers.Remove(soldiers[i]);
           }
       }
            ChainMovement();
     
        AllignStackZ();   
    }
     void AllignStackZ()
    {
        for(int i=0;i<soldiers.Count;i++)
        {
            soldiers[i].transform.position = new Vector3(soldiers[i].transform.position.x,soldiers[i].transform.position.y,soldiers[0].transform.position.z+offset*i);
        }
    }
}
