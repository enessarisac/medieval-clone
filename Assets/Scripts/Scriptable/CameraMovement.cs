using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CameraMovement : MonoBehaviour
{
    Transform player;

    [SerializeField] private Vector3 offset;    
    [SerializeField] private Transform finalPosition;
    [SerializeField] private UnityEvent onFinishLevel;
    bool onFinalPosition;
    private void Start() {
        player = FindObjectOfType<PlayerSwerveMovement>().transform;
    }
    private void Update() 
    {
        if(!onFinalPosition)transform.position = player.position + offset;      
    }
    public void FinishLevel()
    {
        onFinishLevel.Invoke();
    }
    public void FinalPosition()
    {
        onFinalPosition = true;
        PlayerSwerveMovement player = FindObjectOfType<PlayerSwerveMovement>();
        player.slideSpeed=0;
        transform.position = finalPosition.position;
        transform.rotation=finalPosition.rotation;
    }
}
