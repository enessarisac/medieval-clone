using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SwerveInputs : MonoBehaviour
{
    public float startTouch, slideTouch,swipeDelta,touchDistance,speed,slideSpeed,minX,maxX,maxSwipeDelta;
    public Text text;
    private void Start()
    {
        slideSpeed=2;
        minX=-3.7f;
        maxX=5.4f;
    }
    private void FixedUpdate()
    {
        if(Input.touchCount>0)
        { 
            Touch touch = Input.GetTouch(0);
            if(touch.phase == TouchPhase.Began)
            {
                speed=8;
                slideTouch=touch.position.x;
                startTouch = touch.position.x;    
            }
            if(touch.phase == TouchPhase.Stationary)
            {
                slideTouch=touch.position.x;
                startTouch = touch.position.x;
            }
            if (touch.phase == TouchPhase.Moved) 
            {      
                swipeDelta = touch.position.x - startTouch;
                startTouch = touch.position.x;        
            } 
            if(touch.phase == TouchPhase.Ended)
            {
                swipeDelta = 0;
            }
            touchDistance=startTouch-slideTouch;    
        }
        text.text=swipeDelta.ToString();
    }
}
