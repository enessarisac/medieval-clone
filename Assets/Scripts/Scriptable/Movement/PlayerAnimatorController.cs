using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimatorController : MonoBehaviour
{
    SwerveInputs swerveInputs;
    Animator animator;
    float speed;

    private void Start() 
    {
        swerveInputs = FindObjectOfType<SwerveInputs>();
    }
    private void Update() 
    {
        speed = swerveInputs.speed;
        animator = swerveInputs.GetComponentInChildren<Animator>();
        if(speed==0)animator.Play("idle");
        else animator.Play("run");        
    }

}
