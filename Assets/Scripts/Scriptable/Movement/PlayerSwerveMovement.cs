using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerSwerveMovement : MonoBehaviour
{
    SwerveInputs swerveInputs;
    public static UnityAction<bool> onPlayerMovementChanged;
    bool isMoving;
    [ReadOnly]
    public float slideSpeed,speed;
    LevelStarted levelManager;
    public float maxSwipeDelta,minSwipeDelta;
    private void Start()
    {
        levelManager = FindObjectOfType<LevelStarted>();
        swerveInputs=FindObjectOfType<SwerveInputs>();
        levelManager.onLevelStarted?.Invoke();
        slideSpeed=swerveInputs.slideSpeed;
    }
    private void FixedUpdate() 
    {
        speed=swerveInputs.speed;
        transform.Translate(Vector3.forward *  speed * Time.deltaTime);
        if(swerveInputs.swipeDelta<maxSwipeDelta && swerveInputs.swipeDelta>0)
        {
            if(swerveInputs.touchDistance>2f && transform.position.x<swerveInputs.maxX)
            {
                transform.Translate(slideSpeed* Vector3.right * swerveInputs.swipeDelta * Time.fixedDeltaTime);
            }  
        }
        else if (swerveInputs.swipeDelta>minSwipeDelta && swerveInputs.swipeDelta<0)
        {
            if(swerveInputs.touchDistance<-2f && transform.position.x>swerveInputs.minX)
            {
                transform.Translate(slideSpeed* Vector3.right * swerveInputs.swipeDelta * Time.fixedDeltaTime);
            }
        }
        
        if(swerveInputs.swipeDelta != 0)
        {
            if(!isMoving)
            {
                isMoving = true;
                onPlayerMovementChanged?.Invoke(true);
            }
        }
        else
        {
            if(isMoving)
            {
                isMoving = false;
                onPlayerMovementChanged?.Invoke(false);
            }
        }
    }
     public void PositionToZero()
    {
        Vector3 position = transform.position;
        position.x = 0;
        transform.position = position;    
    }
}
