using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{
    SoldierData soldierData;
    private void Awake() 
    {
        soldierData=this.GetComponent<SoldierData>();
    }
     private void OnTriggerEnter(Collider other) 
    {
         if(other.gameObject.tag == "Soldier")
        {  
            if(!Collect.instance.soldiers.Contains(other.gameObject))
            {
                other.GetComponent<BoxCollider>().isTrigger = false;
                other.gameObject.tag = "Untagged";
                other.gameObject.name = "Soldier"+Collect.instance.soldiers.Count;
                other.gameObject.AddComponent<Collision>();
                Collect.instance.AddSoldier(other.gameObject);
            }
            
            
        } 
        if(other.gameObject.tag=="Level Up")
            {
                soldierData.LevelUp(this.gameObject);
                
            }   
        if(other.gameObject.tag=="Level Down")
            {
                soldierData.LevelDown(this.gameObject,1);
                
            } 
        if(other.gameObject.tag=="Obstacle")
        {
            Collect.instance.SeperateSoldier(this.gameObject,this.soldierData.index+1);
        }  
        if(other.gameObject.tag=="Enemy")
        {
            other.GetComponent<Enemy>().OnTakeDamage(this.gameObject,this.soldierData.level);
        }
        if(other.gameObject.tag=="Finish")
        {
            var cam = FindObjectOfType<CameraMovement>();
            cam.FinishLevel();
            FindObjectOfType<PlayerSwerveMovement>().PositionToZero();
        }
    }
}
