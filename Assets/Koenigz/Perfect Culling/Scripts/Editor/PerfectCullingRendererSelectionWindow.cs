﻿// Perfect Culling (C) 2021 Patrick König
//

#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Koenigz.PerfectCulling;
using UnityEditor;
using UnityEngine;

namespace Koenigz.PerfectCulling
{
    public class PerfectCullingRendererSelectionWindow : EditorWindow
    {
        public PerfectCullingBakingBehaviour attachedBakingBehaviour;

        private bool m_onlyEnabled = true;
        private bool m_onlyStatic = false;
        private bool m_includeChildren = true;
        
        private HashSet<Renderer> m_selectedRenderers = new HashSet<Renderer>();
        private Vector2 m_rendererScrollView;

        bool RendererFilter(Renderer renderer)
        {
            if (renderer == null)
            {
                return false;
            }

            if (m_onlyEnabled && (!renderer.enabled || !renderer.gameObject.activeInHierarchy))
            {
                return false;
            }

            if (m_onlyStatic && !renderer.gameObject.isStatic)
            {
                return false;
            }
            
            if (!PerfectCullingConstants.SupportedRendererTypes.Contains(renderer.GetType()))
            {
                return false;
            }

            PerfectCullingRendererTag rendererTag = renderer.GetComponent<PerfectCullingRendererTag>();

            if (rendererTag != null && rendererTag.ExcludeRendererFromBake)
            {
                return false;
            }

            return true;
        }
        
        void OnSelectionChange() 
        {
            m_selectedRenderers.Clear();
            
            foreach (var selectedGameObject in UnityEditor.Selection.gameObjects)
            {
                List<Renderer> renderers = new List<Renderer>();

                if (m_includeChildren)
                {
                    selectedGameObject.GetComponentsInChildren<Renderer>(true, renderers);
                }
                else
                {
                    Renderer renderer = selectedGameObject.GetComponent<Renderer>();

                    if (renderer != null)
                    {
                        renderers.Add(renderer);
                    }
                } 
                
                if (renderers == null || renderers.Count <= 0)
                {
                    continue;
                }

                renderers.RemoveAll((rend) => !RendererFilter(rend));

                if (attachedBakingBehaviour.additionalOccluders != null)
                {
                    HashSet<Renderer> additionalOccluders =
                        new HashSet<Renderer>(attachedBakingBehaviour.additionalOccluders);

                    for (int i = renderers.Count - 1; i >= 0; --i)
                    {
                        if (additionalOccluders.Contains(renderers[i]))
                        {
                            renderers.RemoveAt(i);
                        }
                    }
                }

                foreach (Renderer r in renderers)
                {
                    m_selectedRenderers.Add(r);
                }
            }
            
            Repaint();
        }
        
        private void OnGUI()
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            {
                GUILayout.Label("Target:");
                attachedBakingBehaviour =
                    (PerfectCullingBakingBehaviour) EditorGUILayout.ObjectField(attachedBakingBehaviour,
                        typeof(PerfectCullingBakingBehaviour), true);

                GUI.enabled = attachedBakingBehaviour != null && m_selectedRenderers.Count > 0;
                if (GUILayout.Button($"Add selected renderers ({m_selectedRenderers.Count})"))
                {
                    HashSet<PerfectCullingBakeGroup> result =
                        new HashSet<PerfectCullingBakeGroup>(attachedBakingBehaviour.bakeGroups,
                            new PerfectCullingBakeGroupComparer());

                    foreach (PerfectCullingBakeGroup newBakeGroup in PerfectCullingUtil.CreateBakeGroupsForRenderers(
                        m_selectedRenderers.ToList(), RendererFilter))
                    {
                        result.Add(newBakeGroup);
                    }

                    Undo.RecordObject(attachedBakingBehaviour, "Added renderers");
                    attachedBakingBehaviour.bakeGroups = result.ToArray();

                    UnityEditor.EditorUtility.SetDirty(attachedBakingBehaviour);

                    UnityEditor.Selection.activeObject = attachedBakingBehaviour;
                }

                GUI.enabled = true;

                EditorGUI.BeginChangeCheck();

                m_onlyEnabled = EditorGUILayout.Toggle("Only enabled renderers", m_onlyEnabled);
                m_onlyStatic = EditorGUILayout.Toggle("Only static objects", m_onlyStatic);
                m_includeChildren = EditorGUILayout.Toggle("Include children", m_includeChildren);

                if (EditorGUI.EndChangeCheck())
                {
                    OnSelectionChange();
                }

                GUILayout.Space(5);

                GUILayout.Label("Selected renderers:", EditorStyles.boldLabel);

                m_rendererScrollView = EditorGUILayout.BeginScrollView(m_rendererScrollView);
                foreach (Renderer r in m_selectedRenderers)
                {
                    GUI.enabled = false;
                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.ObjectField(r, typeof(Renderer), true);
                    }
                    EditorGUILayout.EndHorizontal();
                    GUI.enabled = true;
                }

                EditorGUILayout.EndScrollView();
            }
        }
    }
}
#endif