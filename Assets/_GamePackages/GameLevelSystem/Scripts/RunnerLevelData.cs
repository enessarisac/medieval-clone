using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameLevelSystem
{
    [CreateAssetMenu(fileName = "RunnerLevelData", menuName = "Game Level System/RunnerLevelData")]
    public class RunnerLevelData : LevelData
    {
        public static Action<float> OnLaneSizeChanged;
        public float HalfLaneSize;

        public void SetLaneSize(float size)
        {
            OnLaneSizeChanged?.Invoke(size);
        }
    }
}
