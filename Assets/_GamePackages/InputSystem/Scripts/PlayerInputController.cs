using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputController : Controller
{
    void Start()
    {
        Initialize();
        AgentController.SetInputListener(gameObject.AddComponent<MobileInputListener>());
        AgentController.SetInputListener(gameObject.AddComponent<KeyBoardInputListener>(), true);
    }
}
