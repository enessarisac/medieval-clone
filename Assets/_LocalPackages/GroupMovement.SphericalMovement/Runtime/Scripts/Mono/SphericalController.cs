﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ScriptableSystem;
using Sirenix.OdinInspector;
using UnityEngine;


namespace GroupMovement.SphericalMovement
{

    public class SphericalController : NodeController
    {
      
        public override Node PrefabNode => prefabNode;

        [ShowIf(nameof(initializeOnStart))]
        [SerializeField] int initialNodeCount = 0;
        [SerializeField] IntReference nodeCount;
        public override int InitialNodeCount => initialNodeCount;

        public override int NodeCount => nodeCount.Value;

        public override List<Node> GetNodeList => NodeList.Cast<Node>().ToList();

        public List<SphericalNode> NodeList;
        [ReadOnly] public List<SphericalNode> ActiveNodeList;
        [SerializeField] SphericalNode prefabNode;
        public FloatReference CurrentSphereSize;
        [SerializeField] FloatReference sphereSizeMultiplier;
        [SerializeField] SphereCollider sphereCollider;
        [SerializeField] bool showExcessive=true;


        private void Awake()
        {
            nodeCount.Value = NodeList.Count;
        }
        public override void Initialize()
        {
            CreateInitialNodes();
        }
        protected override void CreateInitialNodes()
        {
            CreatePrefabNodes(InitialNodeCount);
        }

        [Button]
        void UpdateSphareSize()
        {
            CurrentSphereSize.Value = sphereSizeMultiplier.Value*GetSphereRadius(NodeCount);
            sphereCollider.radius = CurrentSphereSize.Value;
        }

        void UpdateNodeActivity()
        {
        //    var count = GetCurrentCountSizeForRadius();
            ActiveNodeList.Clear();
            for (int i = 0; i < NodeList.Count; i++)
            {
                var node = NodeList[i];
                var active = showExcessive||i < maxActiveNodeCount.Value;
                node.gameObject.SetActive(active);
                if(active)
                    ActiveNodeList.Add(node);
            }
        }

        void DesignActiveNodes()
        {
            for (int i = 0; i < ActiveNodeList.Count; i++)
            {
                var node = ActiveNodeList[i];
                var radius = GetSphereRadius(i+1);
                var angle = GetAngle(radius, i);
                var direction = new Vector3(-Mathf.Sin(Mathf.Deg2Rad * angle), 0.0f, Mathf.Cos(Mathf.Deg2Rad * angle));
                node.SetCurrentRadius(radius * sphereSizeMultiplier.Value);
                if(!node.IsNodePositioned)
                    node.SetSphericalPosition(direction * node.GetCurrentRadius());
            }
        }

        [Button]
        float GetAngle(int radius,int index)
        {
            if (radius <= 0) return 0;
            index = index % (radius * 4);
            float anglePerNode = 90f / radius;
            return anglePerNode * index;
        }

        [Button]
        int GetSphereRadius(int count)
        {
            int radius = 0;
            while (GetMaxCountSizeForRadius(radius)< count)
            {
                radius++;
            }
            return Mathf.Max(0,radius);
        }

        [Button]
        int GetMaxCountSizeForRadius(int radius)
        {
            return 1 + 2*radius * (radius + 1) ;
        }

        [Button]
        int GetCurrentCountSizeForRadius()
        {
            return GetMaxCountSizeForRadius(GetSphereRadius(NodeCount));
        }

        [Button]
        public void CreatePrefabNodes(int count, bool update = true)
        {


           StartCoroutine(IECreatePrefabNodes(count, update));
    }

    IEnumerator IECreatePrefabNodes(int count, bool update = true)
    {
            for (int i = 0; i < count; i++)
            {
                yield return null;
                CreatePrefabNode(false);
            }
            if (update)
                OnUpdated();

    }


    public override void AddNode(Node node, bool update = true)
        {

            if (maxNodeCount.Value <= NodeCount) return;

            var sphericalNode = (SphericalNode)node;
            NodeList.Add(sphericalNode);
            node.transform.parent = nodeParent;
            sphericalNode.NodeController = this;
            node.OnNodeAdded?.Invoke();
            nodeCount.Value = NodeList.Count;
            OnUpdated();
        }

        [Button]
        public void RemoveLastNodes(int count, bool update = true)
        {
            StartCoroutine(IERemoveLastNodes(count, update));
        }

        IEnumerator IERemoveLastNodes(int count, bool update = true)
        {
            for (int i = 0; i < count; i++)
            {
                yield return null;
                RemoveLastNode(false);
            }
            if (update)
                OnUpdated();
        }

        [Button]
        public void DestroyLastNodes(int count, bool update = true)
        {
            StartCoroutine(IEDestroyLastNodes(count, update));
        }

        IEnumerator IEDestroyLastNodes(int count, bool update = true)
        {
            for (int i = 0; i < count; i++)
            {
                yield return null;
                DestroyLastNode(false);
            }
            if (update)
                OnUpdated();
        }

        public void DestroyLastNode(bool update = true)
        {
            if (NodeCount <= 0) return;
            DestroyNode(NodeList.Last(), update);
        }

        public override void RemoveLastNode(bool update = true)
        {
            if (NodeCount <= 0) return;
            RemoveNode(NodeList.Last(),update);
        }

        public override void RemoveNode(Node node, bool update = true)
        {
            var sphericalNode = (SphericalNode)node;
            NodeList.Remove(sphericalNode);
            ActiveNodeList.Remove(sphericalNode);
            sphericalNode.CallOnNodeRemoved();
            nodeCount.Value = NodeList.Count;
            if (NodeCount <= 0)
                OnNodeCountBelowZero?.Invoke();
            if (update)
                OnUpdated();
        }



        public override void DestroyNode(Node node, bool update = true)
        {
            var sphericalNode = (SphericalNode)node;
            NodeList.Remove(sphericalNode);
            sphericalNode.CallOnNodeDestroyed();
            nodeCount.Value = NodeList.Count;
            if (NodeCount <= 0)
                OnNodeCountBelowZero?.Invoke();
            if (update)
                OnUpdated();
        }

        protected override void OnUpdated()
        {
            UpdateSphareSize();
            UpdateNodeActivity();
            DesignActiveNodes();
        }

    }
}