using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using ScriptableSystem;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace DoorSystem
{
    public abstract class DecisionDoor : MonoBehaviour
    {
        public Action OnDoorHit;
        [SerializeField] StringReference targetReference;
        [SerializeField] UnityEvent onDoorHitEvent;
        [SerializeField] UnityEvent<GameObject> onDoorHitEventObject;

        protected virtual void OnEnable()
        {
            ToggleDisableActions(true);
        }

        protected virtual void OnDisable()
        {
            ToggleDisableActions(false);
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag(targetReference.Value))
            {
                OnDoorHit?.Invoke();
                onDoorHitEvent?.Invoke();
                onDoorHitEventObject?.Invoke(other.gameObject);
                OnDoorEntered(other.gameObject);
            }
        }
        protected virtual void DisableDoor()
        {
            Debug.Log("Inside disable door");
            var collider = GetComponent<Collider>();
            if(collider)
                collider.enabled = false;
        }

        protected abstract void OnDoorEntered(GameObject hitObject);

        void ToggleDisableActions(bool value)
        {
            var siblingDoors = GetSiblingDoors();
            if(siblingDoors == null) return;
            Debug.Log("Sibling hit actions count: " + siblingDoors.Length);
            if(value)
                for (int i = 0; i < siblingDoors.Length; ++i)
                {
                    siblingDoors[i].OnDoorHit += DisableDoor;
                }
            else
                for (int i = 0; i < siblingDoors.Length; ++i)
                {
                    siblingDoors[i].OnDoorHit -= DisableDoor;
                }
        }

        
        DecisionDoor[] GetSiblingDoors()
        {
            if(transform.parent == null) return null;
            DecisionDoor[] doors = transform.parent.GetComponentsInChildren<DecisionDoor>();
            return doors;
            //if(doors.Length > 0)
            // List<Action> siblingHitActions = new List<Action>();
            // for (int i = 0; i < decisionDoors.Length; i++)
            // {
            //     var door = decisionDoors[i];
            //     siblingHitActions.Add(door.OnDoorHit);
            // }
            // return siblingHitActions;
        }

    }   
}