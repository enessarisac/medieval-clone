using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace  GameLevelSystem
{
    public class LevelController : MonoBehaviour
    {
        public static Action OnLevelPassed;
        public static Action OnLevelFailed;
        public static Action OnLevelReloadStarted;
        public LevelData LevelData;

        private void Awake() 
        {
            LevelManager.CurrentLevelController = this;
        }

        protected virtual void Start()
        {
            GameStateManager.OnGameLevelLoaded?.Invoke();
        }
    }    
}