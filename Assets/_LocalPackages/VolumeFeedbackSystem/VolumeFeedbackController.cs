using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Sirenix.OdinInspector;

public class VolumeFeedbackController : MonoBehaviour
{
    [SerializeField] AnimationCurve curve;
    Volume ppVolume;

    [SerializeField] float duration = 0.2f;
    [SerializeField] float maxVal = 1;
    float startTime;
    void Start()
    {
        ppVolume = GetComponent<Volume>();
       

    }

    [Button]
    public void StartVolumeFlash()
    {
        startTime = Time.time;
        StartCoroutine(IEVolumeFlash());
        Invoke(nameof(ResetValue), duration * 1.2f);
    }

    IEnumerator IEVolumeFlash()
    {
        while(Time.time < (startTime + duration))
        {
            ppVolume.weight = curve.Evaluate((Time.time - startTime) / duration);

            yield return null;
        }
    }

    private void ResetValue()
    {
        ppVolume.weight = 0;
    }
}
