﻿using System;
using UnityEngine;

namespace AISystem
{
    public abstract class BotState
    {
        protected BotStateMachine botStateMachine;
        protected GameObject gameObject;
        protected Transform transform;
        protected float stateStartTime;

        public BotState(BotStateMachine botStateMachine, GameObject gameObject)
        {
            this.botStateMachine = botStateMachine;
            this.gameObject = gameObject;
            this.transform = gameObject.transform;
        }

        public virtual Type Tick()
        {
            return null;
        }

        public virtual void OnStateEnter()
        {
            stateStartTime = Time.time;
        }

        public virtual void OnStateUpdate()
        {

        }

        public virtual void OnStateExit()
        {

        }
    }
}