using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AISystem
{
    public class BotWanderState : BotState
    {
        Bot bot;
        Transform chaseTarget;
        float randomWaitTime;
        private Vector3? destination;
        private float walkPointStartTime;

        public BotWanderState(Bot bot) : base(bot.BotStateMachine, bot.gameObject)
        {
            this.bot = bot;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            TryFindRandomDestination();
        }

        public override void OnStateUpdate()
        {
            TryFindRandomDestination();

            if (Time.time - walkPointStartTime >= 5f)
            {
                RemoveDestination();
                botStateMachine.SetNewState(bot.GetDefaultState());
            }
            else if (Time.time - stateStartTime >= 2.25f) //check values in the idle state
            {
                botStateMachine.SetNewState(bot.GetDefaultState());
            }
            else if (destination.HasValue && Vector3.Distance(transform.position, destination.Value) <= 1f) //reached destination
            {
                RemoveDestination();
                botStateMachine.SetNewState(bot.GetDefaultState());
            }
        }

        private void TryFindRandomDestination()
        {
            if (!destination.HasValue)
            {
                SearchWalkPoint();
            }
        }

        private void SearchWalkPoint()
        {
            //calculate random point in range
            float randomZ = UnityEngine.Random.Range(-bot.walkPointRange, bot.walkPointRange);
            float randomX = UnityEngine.Random.Range(-bot.walkPointRange, bot.walkPointRange);
            destination = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);
            if (bot.AIMover.IsValidLocation(destination.Value))
            {
                walkPointStartTime = Time.time;
                bot.AIIntentController.SendMovePosition(transform.position, destination.Value);
            }
            else
            {
                //Debug.Log("Destination=null");
                destination = null;
            }
        }

        private void RemoveDestination()
        {
            destination = null;
        }
    }
}