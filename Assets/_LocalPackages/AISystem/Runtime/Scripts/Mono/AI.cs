using AgentSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AISystem
{
    //[RequireComponent(typeof(AgentController))]
    public class AI : Controller
    {
        public Action<Vector2> OnMoving;
        public Action OnMoveRelased;
        [ReadOnly]    
        public Sensor AISensor;
        [ReadOnly]
        public AIMover AIMover;
        [ReadOnly]
        public AIIntentController AIIntentController;

        public override void Initialize()
        {
            base.Initialize();
            AddSensors();
            AIIntentController = GetComponent<AIIntentController>();
            if(AIIntentController == null)
                AIIntentController = gameObject.AddComponent<AIIntentController>();
        }

        public virtual void MoveToPosition(Vector3 position)
        {
        }

        public virtual void ReleaseMove()
        {
        }

        public virtual void WeaponAim(Vector3 target)
        {
        }
        
        public virtual void AddSensors()
        {
            AISensor = gameObject.AddComponent<Sensor>();
            AIMover = gameObject.AddComponent<AIMover>();
        }
    }
}