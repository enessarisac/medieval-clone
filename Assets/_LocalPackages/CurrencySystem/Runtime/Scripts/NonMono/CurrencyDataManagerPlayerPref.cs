﻿using System.Threading.Tasks;
using UnityEngine;

namespace CurrencySystem
{

    class CurrencyDataManagerPlayerPref : CurrencyDataManager
    {

        string saveKey { get { return currencyType + " SaveData"; } }

        public CurrencyDataManagerPlayerPref(CurrencyType type) : base(type)
        {
        }

        public override void SaveCurrency(CurrencyData data)
        {
            PlayerPrefs.SetString(saveKey, JsonUtility.ToJson(data));
        }

        public async override Task LoadCurrencyDataAsync()
        {
            Debug.Log("Using PlayerPrefs Data");
            await Task.Run(()=> { CurrencyAmount = JsonUtility.FromJson<float>(PlayerPrefs.GetString(saveKey)); });
        }

        public override bool HasDefaultData()
        {
            return PlayerPrefs.HasKey(saveKey);
        }
    }

}