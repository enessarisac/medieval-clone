﻿using System;
using System.IO;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace CurrencySystem
{


    [Serializable]
    public class CurrencyDataManagerFileSystem : CurrencyDataManager
    {
        string folderPath { get;  set; }
        string FilePath { get ; set; }

        public CurrencyDataManagerFileSystem(CurrencyType currencyData) : base(currencyData)
        {
            SetPath();
        }

        private void SetPath()
        {
            folderPath = Application.streamingAssetsPath + "/Currency/";
            FilePath = folderPath + currencyType + " SaveData.txt";
        }

        public override void SaveCurrency(CurrencyData data)
        {
            if (FilePath == null)
                SetPath();
            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);
            Debug.Log("Currency Type " + currencyType);
            {
                using (Stream stream = File.OpenWrite(FilePath))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    var jsonData = JsonUtility.ToJson(data);
                    formatter.Serialize(stream, jsonData);
                }
            }
        }

        public async override Task LoadCurrencyDataAsync()
        {
            Debug.Log("Using FileSystem Data");

            await Task.Run(() =>
            {

                try
                {
                    using (Stream stream = File.OpenRead(FilePath))
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        var jsonData = formatter.Deserialize(stream) as string;
                        CurrencyAmount = JsonUtility.FromJson<float>(jsonData);
                    }
                }
                catch (Exception e)
                {
                    Debug.Log(e.Message);

                }

            });
        }

        public override bool HasDefaultData()
        {
            return File.Exists(FilePath);
        }
    }
}