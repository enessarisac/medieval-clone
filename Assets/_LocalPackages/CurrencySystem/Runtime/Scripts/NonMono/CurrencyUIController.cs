﻿using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;

namespace CurrencySystem
{
    class CurrencyUIController
    {
        Currency currency;
        List<TMP_Text> tmpTexts = new List<TMP_Text>();
        List<Text> unityTexts = new List<Text>();

        public CurrencyUIController(Currency currency)
        {
            this.currency = currency;
            currency.CurrencyType.OnCurrencyUpdated += UpdateUI;
        }

        void UpdateUI()
        {
            var value = currency.GetAmount();
            foreach (var Text in tmpTexts)
            {
                Text.text = value.ToString();
            }
            foreach (var Text in tmpTexts)
            {
                Text.text = value.ToString();
            }
        }

        public void AddCurrencyText(object text)
        {
            if(text is TMP_Text)
                tmpTexts.Add((TMP_Text)text);
            if (text is Text)
                unityTexts.Add((Text)text);
        }

        public void RemoveCurrenncyText(object text)
        {
            if (text is TMP_Text&& tmpTexts.Contains((TMP_Text)text))
                tmpTexts.Remove((TMP_Text)text);
            if (text is Text && unityTexts.Contains((Text)text))
                if (text is Text)
                unityTexts.Remove((Text)text);
        }
    }



}

