using System.Diagnostics;
using TMPro;


namespace CurrencySystem
{
    public class Currency 
    {
        CurrencyUIController currencyUIController;
        public CurrencyType CurrencyType { get; }

        public Currency(CurrencyType currencyType, int ownerID=0)
        {
            CurrencyType = currencyType;
            currencyUIController = new CurrencyUIController(this);
        }

        public void IncreaseAmount(float value)
        {
            SetCurrencyAmount(CurrencyType.Amount + value);
        }

        public void RemoveAmount(float value)
        {
            SetCurrencyAmount(CurrencyType.Amount - value);
        }

        public void SetCurrencyAmount(float value)
        {
            CurrencyType.Amount = value;
            UpdateCurrencyData();
        }

        public void UpdateCurrencyData()
        {
            CurrencyType.OnCurrencyUpdated?.Invoke();     
        }

        public float GetAmount()
        {
            return CurrencyType.Amount;
        }

        public void AddText(params object[] texts)
        {
            foreach (var text in texts)
            {
                currencyUIController.AddCurrencyText(text);
            }
        }

        public void RemoveText(params TMP_Text[] tmpTexts)
        {
            foreach (var text in tmpTexts)
            {
                currencyUIController.AddCurrencyText(text);
            }
        }
    }
}