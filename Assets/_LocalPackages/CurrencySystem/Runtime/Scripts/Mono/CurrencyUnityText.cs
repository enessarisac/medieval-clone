﻿using UnityEngine;
using UnityEngine.UI;

namespace CurrencySystem
{
    [RequireComponent(typeof(Text))]
    public class CurrencyUnityText : CurrencyText
    {
        public override void AddTextToCurrency()
        {
            currency.AddText(GetComponent<Text>());
        }

    }


}
