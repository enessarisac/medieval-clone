﻿namespace ProgressSystem
{
    public class TotalProgressBar : ProgressBar
    {
        private void Start()
        {
            if(setOnStart)
            SetBar(progressType.TotalProgressRatio);
        }
        protected override void OnTotalProgressRatioChanged(float ratio)
        {
            SetBar(ratio);
        }

        public void Initialize()
        {
            SetBar(progressType.TotalProgressRatio);
        }
    }
}