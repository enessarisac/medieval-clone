using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ProgressSystem
{
    public abstract class ProgressBar : ProgressEventListener
    {
        [SerializeField] protected Slider slider;
        [SerializeField] protected bool moveBackward = true;
        [SerializeField] protected bool setOnStart = true;

        public void SetBar(float percentage)
        {
            if (slider == null) return;
            if (moveBackward)
                slider.value = (1 - percentage) * slider.maxValue;
            else
                slider.value = percentage * slider.maxValue;
        }
    }


}