﻿using UnityEngine;
using UnityEngine.Events;

namespace ProgressSystem
{
    public class LevelChangedEvent : ProgressEventListener
    {
        [SerializeField] UnityEvent defaultValueSetEvent;
        [SerializeField] LevelChangedEventData[] levelEvents;
        [SerializeField] UnityEvent onEveryLevelDown;
        [SerializeField] UnityEvent onEveryLevelUp;
        [SerializeField] UnityEvent onProgressMax;
        bool reachedMax = false;

        private void OnValidate()
        {
            for (int i = 0; i < levelEvents.Length; i++)
            {
                levelEvents[i].Index = i + 1;
            }
        }

        private void Start()
        {
            defaultValueSetEvent?.Invoke();
        }

        protected override void OnLevelDown()
        {
            var index = Mathf.Clamp(progressType.CurrentLevel, 0, levelEvents.Length - 1);
            levelEvents[index].LevelDownEvent?.Invoke();
            onEveryLevelDown?.Invoke();
        }

        protected override void OnLevelUp()
        {
            var index =  Mathf.Clamp(progressType.CurrentLevel-1, 0, levelEvents.Length-1);
            levelEvents[index].LevelUpEvent?.Invoke();
            onEveryLevelUp?.Invoke();
            if(progressType.TotalCurrentExp / progressType.TotalMaxExp == 1)
                onProgressMax?.Invoke();
        }

        protected override void OnTotalProgressRatioChanged(float ratio)
        {
            if(reachedMax) return;
            
            if(ratio == 1)
            {
                reachedMax = true;
                onProgressMax?.Invoke();
            }
            else
                reachedMax = false;
        }

    }
}