using System.Data.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;


namespace ProgressSystem
{
    //DebugSystem ine taşı
    public class ProgressTypeDebugger : MonoBehaviour
    {
        [SerializeField] float startOffset;
        [SerializeField] ProgressTypeDebuggerData[] datas;

       
        void ShowProgressData(ProgressTypeDebuggerData data)
        {
            if (data.HideUI) return;
            if (data.IsNull()) return;
            GUI.skin.label.fontSize = data.FontSize;
            GUILayout.Label(data.ProgressType.name);
            GUILayout.Label("CurrentLevel: " + data.ProgressType.CurrentLevel);
            GUILayout.Label("CurrentExp: " + data.ProgressType.CurrentExp);
            GUILayout.Label("ProgressRatio: " + data.ProgressType.CurrentProgressRatio);
            GUILayout.Label("Total Progress ratio listener cnt: " + data.ProgressType.OnTotalProgressRatioChanged.GetPersistentEventCount());
            if (GUILayout.Button("Level Down"))
            {
                ProgressType.LevelDown(data.ProgressType);
            }
            if (GUILayout.Button("Level Up"))
            {
                ProgressType.LevelUp(data.ProgressType);
            }
            // data.ProgressType.OnTotalProgressRatioChanged.GetPersistentEventCount
        }

        private void ShowAllData()
        {
            GUILayout.Space(startOffset);
            for (int i = 0; i < datas.Length; i++)
            {
                ShowProgressData(datas[i]);
                GUILayout.Space(5);
            }
        }

        #if UNITY_EDITOR
                void OnGUI()
                {
                    ShowAllData();
                }
        #endif

    }

}