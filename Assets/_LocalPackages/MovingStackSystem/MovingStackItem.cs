using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MovingStackSystem
{
    public class MovingStackItem : MonoBehaviour
    {
        public Transform StackBase;
        public int stackIndex = 0;
        float smoothStep = 300;
        [SerializeField] float rotationForce = 7;
        public float minStep, maxStep;
        float cubeHeight = 0.15f;

        float updateRate = 0.015f;

        void FixedUpdate()
        {
            Vector3 targetPos = Vector3.Lerp(StackBase.position + StackBase.up * (float)stackIndex * cubeHeight, transform.position, Mathf.Clamp(smoothStep * Time.deltaTime, 0, 1f));

            Vector3 differenceVector = (transform.position - StackBase.position);

            transform.position = targetPos;

            Vector3 rotationVector = differenceVector * rotationForce;
            transform.rotation = Quaternion.Euler(rotationVector.z, StackBase.rotation.eulerAngles.y, -rotationVector.x);
        }

        public void UpdateIndex(int i, int stackCount)
        {
            float stackHeight = Mathf.Max(stackCount, 20);
            stackIndex = i;
            smoothStep = Mathf.Lerp(minStep, maxStep, (float)i / stackHeight);
        }

    }
}