using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MovingStackSystem;
using DG.Tweening;


namespace MovingStackSystem
{
    public class MovingStackController : MonoBehaviour
    {
        [SerializeField] protected List<MovingStackItem> Stack = new List<MovingStackItem>();

        [SerializeField] float minStep = 25;
        [SerializeField] float maxStep = 50;
        [SerializeField] Transform stackCenterTransform;
        [SerializeField] GameObject stackItemPrefab;

        void Awake()
        {
            //for testing
            //for (int i = transform.childCount-1; i >= 0; i--)
            //{
            //    AddItemToList(transform.GetChild(i).gameObject);
            //    transform.GetChild(i).SetParent(null);
            //}
        }

        [Button("AddItem")]
        public void AddItemToList(GameObject stackItem)
        {

            Debug.Log("Item Added");
            var item = stackItem.AddComponent<MovingStackItem>();
            Stack.Add(item);

            item.UpdateIndex(0, Stack.Count);
            item.minStep = minStep;
            item.maxStep = maxStep;
            item.StackBase = stackCenterTransform;
            
            UpdateIndexes(1);
            
        }

        public void AddItemToList()
        {
            AddItemToList(Instantiate(stackItemPrefab));
            Debug.Log("stack item added");
        }

        public void RemoveFromList()
        {
            if(Stack.Count == 0) return;
            var movingStackItem = Stack[0].GetComponent<MovingStackItem>();
            if(movingStackItem)
            {
                //Destroy(movingStackItem);
                Stack.RemoveAt(0);
                UpdateIndexes(-1);
                Destroy(movingStackItem.gameObject, 3);
                //movingStackItem.gameObject.AddComponent<Rigidbody>().AddForce(Vector3.down *100);
                Vector3 randomVector = new Vector3(movingStackItem.transform.position.x + Random.Range(-1f, 1f), 0, movingStackItem.transform.position.z + Random.Range(-1f, 1f));
                movingStackItem.transform.DOMove(randomVector, 0.7f);
                Destroy(movingStackItem);
            }
        }

        public void RemoveFromList(MovingStackItem movingStackItem)
        {
            if(Stack.Count == 0) return;
            if(Stack.Contains(movingStackItem))
            {
                Stack.Remove(movingStackItem);
                UpdateIndexes(-1);
                Destroy(movingStackItem);
            }
            //var movingStackItem = Stack[0].GetComponent<MovingStackItem>();
            // if(movingStackItem)
            // {
            //     Destroy(movingStackItem);
            //     Stack.RemoveAt(0);
            //     UpdateIndexes(-1);
            // }
        }
        
        [Button("Remove All")]
        public void RemoveAllFromList()
        {
            var stackCount = Stack.Count;
            for (int i = 0; i < stackCount; i++)
            {
                RemoveFromList();
            }
        }

        void UpdateIndexes(int i)
        {
            for (int x = 0; x < Stack.Count; x++)
            {
                Stack[x].UpdateIndex(Stack.Count - x - 1, Stack.Count);
            }
            //foreach(MovingStackItem item in Stack)
            //{
            //    item.UpdateIndex(item.stackIndex += i, Stack.Count);
            //}
        }

        public void SetStackCenter(Transform center)
        {
            stackCenterTransform = center;
        }

        private void OnDestroy()
        {
            RemoveAllFromList();
        }
    }
}