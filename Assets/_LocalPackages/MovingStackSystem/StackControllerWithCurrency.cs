﻿using UnityEngine;
using CurrencySystem;
using ToggleSystem;

namespace MovingStackSystem.WithCurrency
{
    public class StackControllerWithCurrency : MovingStackController
    {
        [SerializeField] CurrencyType currencyType;

        private void OnEnable()
        {
            currencyType.OnCurrencyUpdated += UpdateStack;
        }

        private void OnDisable()
        {
            currencyType.OnCurrencyUpdated -= UpdateStack;
        }

        void UpdateStack()
        {
            int ss = Stack.Count;
            if(currencyType.Amount> Stack.Count)
            {
                for (int i = 0; i < currencyType.Amount - ss; i++)
                {
                    AddItemToList();
                }
            }
            else
            {
                for (int i = 0; i < ss - currencyType.Amount; i++)
                {
                    RemoveFromList();
                }
            }
            ToggleManager.Instance.SetToggle(Stack.Count / 5);
        }
    }
}
