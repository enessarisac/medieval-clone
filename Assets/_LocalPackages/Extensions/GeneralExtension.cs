﻿using System.Collections.Generic;
using UnityEngine;


public static partial class Extension
{
    public static bool IsNull<T>(this T nulableObject,bool showError=false)
    {
        var result = ReferenceEquals(null, nulableObject);
        if (result&& showError)
            Debug.LogError(typeof(T) + "cannot found");

        return result;
    }

    public static T GetLast<T>(this T[] value) 
    {
        return value[value.Length - 1];
    }

    public static T GetLast<T>(this List<T> value)
    {
        return value[value.Count - 1];
    }

    public static T GetRandom<T>(this T[] value)
    {
        return value[Random.Range(0, value.Length)];
    }

    public static T GetRandom<T>(this List<T> value)
    {
        return value[Random.Range(0, value.Count)];
    }
}