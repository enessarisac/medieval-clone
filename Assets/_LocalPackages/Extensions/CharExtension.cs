﻿
public static partial class Extension
{
    public static bool CompareWithoutCaseSensitivity(this char value, char compareChar)
    {
        return char.ToLower(value) == char.ToLower(compareChar);
    }
}