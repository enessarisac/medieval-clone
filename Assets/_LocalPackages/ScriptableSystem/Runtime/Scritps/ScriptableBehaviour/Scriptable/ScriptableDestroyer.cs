﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem.ScriptableBehaviour
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableBehaviour/ScriptableDestroyer")]
    public class ScriptableDestroyer : ScriptableObject
    {
        [SerializeField] float delayTime = 0f;
        [SerializeField] UnityEvent<GameObject> onDestroyed;


        public void DestroyObject(GameObject go)
        {
            DestroyObject(go, delayTime);
        }

        public void DestroyObject(GameObject go,float delay)
        {
            Destroy(go, delay);
            onDestroyed?.Invoke(go);
        }
    }
}

