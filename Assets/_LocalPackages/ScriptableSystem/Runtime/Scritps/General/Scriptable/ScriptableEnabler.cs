﻿using System;
using UnityEditor;
using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(fileName = "Enabler", menuName = "ScriptableSystem/ScriptableEnabler")]
    public class ScriptableEnabler : ScriptableObject
    {
        public Action Activate;
        public Action Deactivate;

        [ContextMenu("Call Activate")]
        public void CallActivate()
        {
            Activate?.Invoke();
        }

        [ContextMenu("Call Deactivate")]
        public void CallDeactivate()
        {
            Deactivate?.Invoke();
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(ScriptableEnabler))]
    public class ScriptableEnablerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            ScriptableEnabler enabler = (ScriptableEnabler)target;
            if (GUILayout.Button("Call Activate"))
            {
                enabler.CallActivate();
            }

            if (GUILayout.Button("Call Deactivate"))
            {
                enabler.CallDeactivate();
            }
        }
    }
#endif

}