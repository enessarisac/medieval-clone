﻿using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(fileName = "KeepOneData", menuName = "ScriptableSystem/ScriptableKeepOneData ")]
    public class ScriptableKeepOneData : ScriptableObject
    {

    }


}
