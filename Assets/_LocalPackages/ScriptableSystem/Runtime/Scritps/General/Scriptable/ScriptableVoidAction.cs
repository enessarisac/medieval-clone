﻿using System;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    [CreateAssetMenu(fileName = "VoidAction", menuName = "ScriptableSystem/ScriptableAction/ScriptableVoidAction")]
    public class ScriptableVoidAction : ScriptableObject
    {
        [ReadOnly] public UnityEvent Action;
        [SerializeField] float cooldownTime = 0;
        [ReadOnly] [SerializeField] Cooldown cooldown;

        private void OnValidate()
        {
            SetCooldown();
        }

        private void Awake()
        {
            SetCooldown();
        }

        private void SetCooldown()
        {
            cooldown = new Cooldown(cooldownTime);
            cooldown.UpdateCooldown();
        }

        [Button("Call Action")]
        public void CallAction()
        {
            if(cooldownTime<=0)
            {
                Action?.Invoke();
                cooldown.UpdateCooldown();
            }
            else if(cooldown.IsCooldownFinished)
            {
                Action?.Invoke();
                cooldown.UpdateCooldown();
            }
        }
    }

}