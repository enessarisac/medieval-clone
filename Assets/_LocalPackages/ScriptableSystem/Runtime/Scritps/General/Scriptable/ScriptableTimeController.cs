using UnityEngine;
namespace ScriptableSystem
{
    [CreateAssetMenu(fileName = "TimeController", menuName = "ScriptableSystem/TimeController")]
    public class ScriptableTimeController : ScriptableObject
    {
        public void SetTime(float time)
        {
            Time.timeScale = time;
        }
    }

}