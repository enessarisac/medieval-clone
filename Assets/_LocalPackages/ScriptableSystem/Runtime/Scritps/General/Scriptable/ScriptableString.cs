﻿using Newtonsoft.Json;
using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(menuName ="ScriptableSystem/ScriptableData/ScriptableString")]
    public class ScriptableString : ScriptableData
    {
        [SerializeField] string value;
        public override object Value { get { return value; } protected set { this.value = (string)value; } }

        public string GetValue() => (string)Value;


        public override void Load()
        {
            var loadData = JsonConvert.DeserializeObject<string>(PlayerPrefs.GetString(saveKey, ""));
            UpdateValue(loadData);
        }

    }
}

