﻿using Newtonsoft.Json;
using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableData/ScriptableSprite")]
    public class ScriptableSprite : ScriptableData
    {
        [SerializeField] Sprite value;

        public override object Value { get { return value; } protected set { this.value = (Sprite)value; } }
        public Sprite GetValue() => (Sprite)Value;

        public override void Load()
        {
            var loadData = JsonConvert.DeserializeObject<Sprite>(PlayerPrefs.GetString(saveKey, ""));
            UpdateValue(loadData);
        }
    }
}
