using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
namespace ScriptableSystem
{
    public abstract class ScriptableData : ScriptableObject
    {
        public abstract object Value { get; protected set; }
        [Toggle("Enabled")]
        [SerializeField] protected SaveToggleable saveData;
        protected string saveKey { get { return saveData.SaveKey.Value; } }
        public Action OnValueUpdated;

        private void OnValidate()
        {
            OnValueUpdated?.Invoke();
        }

        private void Start()
        {
            if (saveData.Enabled && HasKey())
                Load();
        }

        public void UpdateValue(ScriptableData data)
        {
            UpdateValue(data.Value);
        }

        protected virtual void IncreaseValue(ScriptableData data) { }
        protected virtual void DecreaseValue(ScriptableData data) { }

        public virtual void UpdateValue(object value,bool callUpdated=true)
        {
            if (Value == value) return;
            Value = value;
            if(callUpdated)
                OnValueUpdated?.Invoke();
            if (saveData.Enabled)
                Save();
        }
        [Button]
        public void Save()
        {
            if (HasKey())
            {
                var json = JsonConvert.SerializeObject(Value);
                PlayerPrefs.SetString(saveKey, json);
            }
        }
        protected bool HasKey()
        {
            if (string.IsNullOrEmpty(saveKey))
            {
                Debug.LogError(name + " SaveKey is empty");
                return false;
            }
            else
                return true;
        }
        [Button]
        public abstract void Load();
        [Button]
        public void CallUpdate()
        {
            UpdateValue(Value);
        }

        [Serializable]
        protected class SaveToggleable
        {
            public bool Enabled;
            public StringReference SaveKey;
        }

    }
}