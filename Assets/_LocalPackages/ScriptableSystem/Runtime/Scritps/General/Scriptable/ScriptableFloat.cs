﻿using System;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ScriptableSystem
{
    [Serializable]
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableData/ScriptableFloat")]
    public partial class ScriptableFloat : ScriptableData
    {
        [Toggle("Enabled")]
        [SerializeField] FloatValueToggleable minValue;
        [Toggle("Enabled")]
        [SerializeField] FloatValueToggleable maxValue;
        [SerializeField] float value;
        public override object Value { get { return value; } protected set { this.value = (float)value; } }
        public float GetValue() => (float)Value;
        public Action<float> OnValueIncreased;
        public Action<float> OnValueDecreased;


        public override void Load()
        {
                var loadData= JsonConvert.DeserializeObject<float>(PlayerPrefs.GetString(saveKey, ""));
                UpdateValue(loadData); 
        }

        public void IncreaseValue(ScriptableFloat v)
        {
            IncreaseValue(v.value);
        }

        public void IncreaseValue(float v)
        {
            OnValueIncreased?.Invoke(v);
            UpdateValue(value + v);

        }

        public void DecreaseValue(ScriptableFloat v)
        {
            DecreaseValue(v.value);
        }

        public void DecreaseValue(float v)
        {
            OnValueDecreased?.Invoke(v);
            UpdateValue(value - v);
        }

        public void SetValue(float v)
        {
            OnValueDecreased?.Invoke(v);
            UpdateValue(v);
        }
        public override void UpdateValue(object value, bool callUpdated = true)
        {
            if (minValue.Enabled)
                value = Mathf.Max(minValue.Value, (float)value);
            if (maxValue.Enabled)
                value = Mathf.Min(maxValue.Value, (float)value);
            base.UpdateValue(value);
        }
    }
}