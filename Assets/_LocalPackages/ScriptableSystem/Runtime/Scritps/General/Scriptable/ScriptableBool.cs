﻿using Newtonsoft.Json;
using UnityEngine;
namespace ScriptableSystem
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableData/ScriptableBool")]
    public class ScriptableBool : ScriptableData
    {
        [SerializeField] bool value;

        public override object Value { get { return value; } protected set { this.value = (bool)value; } }
        public bool GetValue() => (bool)Value;

        public override void Load()
        {
            var loadData = JsonConvert.DeserializeObject<bool>(PlayerPrefs.GetString(saveKey, ""));
            UpdateValue(loadData);
        }
    }
}
