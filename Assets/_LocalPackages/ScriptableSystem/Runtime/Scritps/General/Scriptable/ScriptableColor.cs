using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(fileName = "ScriptableColor", menuName = "ScriptableSystem/ScriptableColor")]
    public class ScriptableColor : ScriptableData
    {
        public Color value=Color.black;
        public override object Value { get { return value; } protected set { this.value = (Color)value; } }
        public Color GetValue() => (Color)Value;

        public override void Load()
        {
            var loadData = JsonConvert.DeserializeObject<Color>(PlayerPrefs.GetString(saveKey, ""));
            UpdateValue(loadData);
        }
    }
}