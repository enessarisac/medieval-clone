﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    [CreateAssetMenu(fileName = "EventListener", menuName = "ScriptableSystem/ScriptableEventListener")]
    public class ScriptableEventListener : ScriptableObject
    {
        public UnityEvent OnEventCalled;


        public void CallAction()
        {
            
            OnEventCalled?.Invoke();
        }
    }

}