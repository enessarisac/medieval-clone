﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    [AddComponentMenu("LocalPackages/ScriptableSystem/MonoDataEventSetter/MonoVector3EventSetter",0)]
    public class MonoVectorEventSetter : MonoDataEventSetter
    {
        [SerializeField] UnityEvent<Vector3> unityEvent;

        protected override void UpdateValue()
        {
            unityEvent?.Invoke((Vector3)data.Value);
        }
    }
}
