﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    [AddComponentMenu("LocalPackages/ScriptableSystem/MonoDataEventSetter/MonoSpriteEventSetter", 0)]
    public class MonoSpriteEventSetter : MonoDataEventSetter
    {
        [SerializeField] UnityEvent<Sprite> unityEvent;

        public override void Initialize()
        {
            base.Initialize();

            UpdateValue();
        }

        public override void DeInitialize()
        {
            if (data.IsNull(true)) return;
            data.OnValueUpdated -= UpdateValue;
        }

        protected override void UpdateValue()
        {
            unityEvent?.Invoke((Sprite)data.Value);
        }
    }



    [AddComponentMenu("LocalPackages/ScriptableSystem/MonoDataEventSetter/MonoIntEventSetter",0)]
    public class MonoIntEventSetter : MonoDataEventSetter
    {
        [SerializeField] UnityEvent<int> unityEvent;
        [SerializeField] UnityEvent<int> onValueIncreased;
        [SerializeField] UnityEvent<int> onValueDecreased;


        public override void Initialize()
        {
            base.Initialize();

            ((ScriptableInt)data).OnValueIncreased+= OnValueIncreased;
            ((ScriptableInt)data).OnValueDecreased+= OnValueDecreased;
            UpdateValue();
        }

        public override void DeInitialize()
        {
            if (data.IsNull(true)) return;
            data.OnValueUpdated -= UpdateValue;
        }


        protected override void OnValueIncreased(int value)
        {
            onValueIncreased?.Invoke((int)data.Value);
        }

        protected override void OnValueDecreased(int value)
        {
            onValueDecreased?.Invoke((int)data.Value);
        }


        protected override void UpdateValue()
        {
            unityEvent?.Invoke((int)data.Value);
        }
    }
}
