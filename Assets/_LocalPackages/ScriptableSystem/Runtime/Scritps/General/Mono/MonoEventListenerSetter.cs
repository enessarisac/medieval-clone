﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    public class MonoEventListenerSetter : MonoSetter
    {
        [SerializeField] ScriptableEventListener eventToListen;
        [SerializeField] UnityEvent OnEventCalled;


        public override void Initialize()
        {
            eventToListen.OnEventCalled.AddListener(CallListeners);
        }

        void CallListeners()
        {
            OnEventCalled?.Invoke();
        }

        public override void DeInitialize()
        {
            eventToListen.OnEventCalled.RemoveListener(CallListeners);
        }

    }

}
