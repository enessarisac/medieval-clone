﻿using TMPro;
using UnityEngine;

namespace ScriptableSystem
{
    public class ScriptableDataText : MonoBehaviour
    {
        [SerializeField] ScriptableData data;
        [SerializeField] string prefix;
        [SerializeField] string suffix;
        [SerializeField] TMP_Text text;

        private void Start()
        {
            UpdateText();
        }

        private void OnValidate()
        {
            data.OnValueUpdated -= UpdateText;
            data.OnValueUpdated += UpdateText;
            UpdateText();
        }

        private void OnEnable()
        {
            data.OnValueUpdated += UpdateText;
        }

        private void OnDisable()
        {
            data.OnValueUpdated -= UpdateText;
        }

        void UpdateText()
        {
            if (text.IsNull(true)) return;
            if (data.IsNull(true)) return;
            text.SetText(prefix + data.Value.ToString() + suffix);
        }

    }

}
