﻿using UnityEngine;

namespace ScriptableSystem
{
    public abstract class MonoDataEventSetter : MonoSetter
    {
        [SerializeField] protected ScriptableData data;
        [SerializeField] bool setOnStart;
  
        private void Start()
        {
            if(setOnStart)
                UpdateValue();
        }

        private void OnValidate()
        {
            DeInitialize();
            Initialize();
        }

        public override void Initialize()
        {
            if (data.IsNull(true)) return;
            data.OnValueUpdated += UpdateValue;
            UpdateValue();
        }

        public override void DeInitialize()
        {
            if (data.IsNull(true)) return;
            data.OnValueUpdated -= UpdateValue;
        }

        protected abstract void UpdateValue();
        protected virtual void OnValueIncreased(int value) { }
        protected virtual void OnValueDecreased(int value) { }

    }
}