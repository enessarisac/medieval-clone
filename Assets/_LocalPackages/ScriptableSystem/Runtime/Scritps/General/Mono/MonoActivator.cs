﻿using UnityEngine;
namespace ScriptableSystem
{
    [AddComponentMenu("LocalPackages/ScriptableSystem/MonoActivator",0)]
    public class MonoActivator : MonoSetter
    {
        [SerializeField] GameObject activateObject;
        [SerializeField] ScriptableActivator scriptableActivator;
        [SerializeField] bool setOnStart;
        [SerializeField] bool setActive;
        [SerializeField] float delayActivateTime;

        public override void DeInitialize()
        {
            scriptableActivator.Activate -= SetActive;
            scriptableActivator.Deactivate -= SetDeactive;
        }

        public override void Initialize()
        {
            if (scriptableActivator == null) return;
            scriptableActivator.Activate += SetActive;
            scriptableActivator.Deactivate += SetDeactive;
            Invoke(nameof(ToggleActivate), delayActivateTime);
        }

        private void ToggleActivate()
        {
            if (!activateObject)
                activateObject = gameObject;
            if (setOnStart)
                activateObject.SetActive(setActive);
        }

        void SetActive()
        {
            activateObject.SetActive(true);
        }

        void SetDeactive()
        {
            if(gameObject)
                activateObject.SetActive(false);
        }
    }
}