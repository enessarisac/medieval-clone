﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    [AddComponentMenu("LocalPackages/ScriptableSystem/MonoDataEventSetter/MonoFloatEventSetter",0)]
    public class MonoFloatEventSetter : MonoDataEventSetter
    {
        [SerializeField] UnityEvent<float> unityEvent;

        protected override void UpdateValue()
        {
            unityEvent?.Invoke((float)data.Value);
        }
    }
}
