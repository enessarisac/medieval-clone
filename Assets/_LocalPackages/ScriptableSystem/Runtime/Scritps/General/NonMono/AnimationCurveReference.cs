﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ScriptableSystem
{
    [Serializable]
    [InlineProperty]
    public class AnimationCurveReference
    {
        [HorizontalGroup(nameof(AnimationCurveReference), MaxWidth = 100)]
        [ValueDropdown("valueList")]
        [HideLabel]
        public bool useValue = true;

        [HideIf("useValue", Animate = true)]
        [HorizontalGroup(nameof(AnimationCurveReference))]
        [HideLabel]
        [Required]
        public ScriptableAnimationCurve variable;

        [ShowIf("useValue", Animate = true)]
        [HorizontalGroup(nameof(AnimationCurveReference))]
        [HideLabel]
        public AnimationCurve constantValue;

        private ValueDropdownList<bool> valueList = new ValueDropdownList<bool>()
        {
            {"Value", true },
            {"Reference",false },
        };

        public AnimationCurve Value
        {
            get
            {
                //return IsNullOrEmpty? "" : useValue ? constantValue : variable.GetValue();
                return useValue ? constantValue : variable.GetValue();
            }
            set
            {
                if (useValue)
                    constantValue = value;
                else
                    variable.UpdateValue(value);
            }
        }
    }
}

