﻿using System;
using System.Collections;

namespace ScriptableSystem
{

    public interface IIEnumerator
    {
        bool HasStarted { get; set; }
        IEnumerator GetIEnumerator();
    }
}
