﻿using System;
using System.Collections.Generic;

namespace ScriptableSystem.ScriptableCondition
{
    [Serializable]
    public class ScriptableConditionList
    {
        [ReadOnly] public bool IsConditionProvided;
        public List<ScriptableCondition> ScriptableConditions;
        public Action OnConditionChanged;
        bool isInitialized;

        public void Initialize()
        {
            if (isInitialized) return;
            for (int i = 0; i < ScriptableConditions.Count; i++)
            {
                ScriptableConditions[i].InitializeCondition();
                ScriptableConditions[i].OnConditionChanged += SetConditon;
            }
            isInitialized = true;
            SetConditon();
        }

        public void Deinitialize()
        {
            if (!isInitialized) return;
            for (int i = 0; i < ScriptableConditions.Count; i++)
            {
                ScriptableConditions[i].DeinitializeCondition();
                ScriptableConditions[i].OnConditionChanged -= SetConditon;
            }
            isInitialized = false;
        }

        void SetConditon()
        {
            bool isConditionProvided = true;
            for (int i = 0; i < ScriptableConditions.Count; i++)
            {
                isConditionProvided = isConditionProvided && ScriptableConditions[i].IsConditionProvided;
            }
            IsConditionProvided = isConditionProvided;
            OnConditionChanged.Invoke();
        }

    }
}