﻿using UnityEditor;
using UnityEngine;

namespace ScriptableSystem.ScriptableCondition
{

    [CreateAssetMenu(fileName = "GeneralCondition", menuName = "ScriptableSystem/ScriptableCondition/GeneralCondition")]
    public class ScriptableGeneralCondition : ScriptableCondition
    {

        void OnValidate()
        {
            OnConditionChanged?.Invoke();
            Debug.Log("Condition status changed");
        }

    }



}