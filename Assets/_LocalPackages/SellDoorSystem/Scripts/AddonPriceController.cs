using System.Collections;
using System.Collections.Generic;
using AddonSystem;
using UnityEngine;

public class AddonPriceController : MonoBehaviour
{
    [ReadOnly] [SerializeField] int currentPrice = 0;
    [SerializeField] int basePrice = 0;

    [SerializeField] AddonPrices addonPrices;
    AddonController addonController;
    AddonController addonControllerGetter 
    { 
        get 
        {
            if (addonController == null)
                addonController = GetComponentInChildren<AddonController>();
            return addonController; 
        } 
    }

    public int GetCurrentPrice()
    {
        currentPrice = basePrice;
        for (int i = 0; i < addonControllerGetter.ActiveAddons.Count; i++)
        {
            currentPrice += addonPrices.GetPrice(addonControllerGetter.ActiveAddons[i]);
        }

        return currentPrice;
    }

}
