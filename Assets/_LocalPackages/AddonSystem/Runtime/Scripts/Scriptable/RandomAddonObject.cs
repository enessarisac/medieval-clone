﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AddonSystem
{
    [CreateAssetMenu(menuName = "AddonSystem/RandomAddonObject")]
    public class RandomAddonObject : ScriptableObject
    {
        [SerializeField] AddonObject[] addons;

        public void AddRandomAddon(GameObject go)
        {
            GetRandomAddon(go).AddAddon(go);
        }

        public void RemoveRandomAddon(GameObject go)
        {
            GetRandomAddon().RemoveAddon(go);
        }

        public void AddRandomAddon(AddonController addonController)
        {
            addonController.TryAddAddon(GetRandomAddon(addonController.gameObject));
        }

        public void RemoveRandomAddon(AddonController addonController)
        {
            addonController.TryRemoveAddon(GetRandomAddon());
        }

        public AddonObject GetRandomAddon()
        {
            return addons[Random.Range(0, addons.Length)];
        }

        public AddonObject GetRandomAddon(GameObject go)
        {
            List<AddonObject> availableAddons = addons.ToList();
            var count = availableAddons.Count;

            for (int i = 0; i < count; i++)
            {
                var addon = availableAddons[i];
                if (addon.HasAddonHasReachedTheCapacity(go))
                {
                    availableAddons.RemoveAt(i);
                    i--;
                    count--;
                }
            }

            return availableAddons[Random.Range(0, availableAddons.Count)];
        }

    }
}
