﻿using System;
using UnityEngine.Events;

namespace AddonSystem
{
    [Serializable]
    public class AddonEffect
    {
        public AddonObject AddonObject;
        [ReadOnly]public int CurrentAddonCount;
        public bool HasReachedTheCapacity { get { return AddonObject.MaxAddonCount <= CurrentAddonCount; } }
        public UnityEvent OnAddonAdded;
        public UnityEvent OnAddonFailedToAdd;
        public UnityEvent OnAddonRemoved;
    }
}