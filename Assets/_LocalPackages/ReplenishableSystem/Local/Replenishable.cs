using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

namespace ReplenishableSystem
{
    public class Replenishable : MonoBehaviour
    {
        [SerializeField]
        float initialValue = 100;
        public float InitialValue { get { return initialValue; } }
        protected float maxValue = 500;
        [ReadOnly]
        [SerializeField]
        float currentValue;
        public float CurrentValue { get { return currentValue; } protected set { currentValue = value; } }
        public Action OnValueBelowZero;
        public Action OnValueChanged;
        public float ReplenishablePercentage { get { return maxValue == 0 ? 0 : 1f * CurrentValue / maxValue; } }


        #region Unity Methods

        private void Awake()
        {
            SetMaxValue(initialValue);
        }

        private void Start()
        {
            SetNewValue(initialValue);
        }
        #endregion

        #region Class Methods
        //private void InitializeReplenishable()
        //{
        //    SetNewValue(initialValue);
        //}

        public virtual void IncreaseValue(int value)
        {
            if (value < 0)
            {
                Debug.Log("Please don't use negative integers");
                return;
            }
            CurrentValue += value;
            SetNewValue(CurrentValue);
        }

        public virtual void DecreaseValue(int value)
        {
            if (value < 0)
            {
                Debug.Log("Please don't use negative integers");
                return;
            }
            if (CurrentValue <= 0)
            {
                //ControllableDebugSystem.Debug.Log("It is already used up. CurrentValue: " + CurrentValue);
                return;
            }
            //ControllableDebugSystem.Debug.Log("Decreasing value by: " + value);
            CurrentValue -= value;
            CurrentValue = Mathf.Max(0, CurrentValue);
            //ControllableDebugSystem.Debug.Log("New health: " + CurrentValue);
            SetNewValue(CurrentValue);


        }

        public void SetReplenishableValue(float value)
        {
            initialValue = value;
            //SetMaxValue(value);

        }

        public void SetMaxValue(float value)
        {
            maxValue = value;
        }

        public float GetMaxValue()
        {
            return maxValue;
        }

        protected virtual void SetNewValue(float value)
        {

        }
        #endregion
    }

}
