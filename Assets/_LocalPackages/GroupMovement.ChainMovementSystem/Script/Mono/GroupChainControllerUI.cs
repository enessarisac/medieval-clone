using UnityEngine;
using TMPro;

namespace GroupMovement.ChainMovementSystem
{
    public class GroupChainControllerUI : MonoBehaviour
    {
        public TextMeshPro NodeCounterTMP;

        [SerializeField] GroupChainController groupChainController;

        private void OnEnable()
        {
            groupChainController.OnNodeCountUpdated += UpdateText;
        }

        private void OnDisable()
        {
            groupChainController.OnNodeCountUpdated -= UpdateText;
        }

        private void UpdateText(int nodeCount)
        {
            if(nodeCount <= 0)
            {
                Destroy(gameObject);
            }

            NodeCounterTMP.SetText(nodeCount.ToString());
        }
    }

}