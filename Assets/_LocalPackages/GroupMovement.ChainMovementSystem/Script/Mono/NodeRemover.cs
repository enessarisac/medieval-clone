using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

namespace GroupMovement.ChainMovementSystem
{

    public class NodeRemover : MonoBehaviour
    {
        
        [SerializeField] bool disableRenderers = false;
        [SerializeField] protected int numberOfRemove;
        [SerializeField] protected UnityEvent<GameObject> onNodeRemoved;
        [SerializeField] protected UnityEvent<GameObject> onNodeRemoverDisabled;
        [SerializeField] bool fallMovement = false;
        [SerializeField] Cooldown cooldown;

        private void Awake()
        {
            if(disableRenderers)
            DisableRenderers();
        }

        private void DisableRenderers()
        {
            var renderers = GetComponentsInChildren<MeshRenderer>();
            foreach (var renderer in renderers)
            {
                renderer.enabled = false;
            }
        }

        public void OnTriggerEnter(Collider other)
        {
            if (!cooldown.IsCooldownFinished) return;
            cooldown.UpdateCooldown();
            var node = other.GetComponent<ChainNode>();
            if (node == null) return;
            if(node.NodeData.Removable)
                RemoveNode(node);
        }

        protected virtual void RemoveNode(ChainNode node)
        {
         
            if (node.NodeController == null) return;
            node.NodeController.RemoveNode(node);
          
            // node.DestroyNode();
            numberOfRemove -= 1;
            Debug.Log(numberOfRemove);
            onNodeRemoved?.Invoke(node.gameObject);
            if(!fallMovement)
                node.gameObject.SetActive(false);
            else
                node.transform.DOLocalMoveY(-10, 2f);

            if (numberOfRemove <= 0)
            {
                GetComponent<Collider>().enabled = false;
                onNodeRemoverDisabled?.Invoke(node.gameObject);
            }
        }


    }

}


