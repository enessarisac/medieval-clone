﻿using UnityEngine;
using UnityEngine.Events;

namespace GroupMovement.ChainMovementSystem
{
    public class NodeAdder : MonoBehaviour
    {
        [SerializeField] ChainNode node;
        [SerializeField] UnityEvent onNodeAdded;

        private void Awake()
        {
            node.GetComponent<Rigidbody>().isKinematic = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            var collidedNode = other.GetComponent<ChainNode>();
            if (collidedNode && collidedNode.NodeController)
            {
                GetComponent<Collider>().enabled = false;
                node.GetComponent<Rigidbody>().isKinematic = false;
                collidedNode.NodeController.AddNode(node);
                onNodeAdded?.Invoke();
            }
        }
    }
}
