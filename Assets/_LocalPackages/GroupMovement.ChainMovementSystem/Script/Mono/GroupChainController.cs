﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Sirenix.OdinInspector;
using ToggleSystem;
using UnityEngine;

namespace GroupMovement.ChainMovementSystem
{
    public class GroupChainController : MonoBehaviour
    {
        [SerializeField] ChainControllerData chainControllerData;
        [SerializeField] ChainController[] chainControllers;
        public int TotalNodeCount
        {
            get
            {
                int total = 0;
                foreach (var controller in chainControllers)
                {
                    total += controller.NodeCount;
                }
                return total;
            }
        }
     
        float lastUpdatedTime;

        int nodesPerController{ get{ return chainControllers.Length == 0 ? 0 : TotalNodeCount / chainControllers.Length; }}

        public ChainNode NodeToFollow;
        public Action<int> OnNodeCountUpdated;
        public static Action<int> OnNodeCountUpdatedStatic;
        [SerializeField] bool checkMaxDistance = true;
        [SerializeField] Vector3 maxDistanceOffset = new Vector3(0, 0, -50);
        [SerializeField] float maxDistance=50;
        [SerializeField] float checkInterval=1;

        private void Start()
        {
            Initialize();
        }

        private void OnEnable()
        {
            OnNodeCountUpdatedStatic += OnNodeCountUpdated;
        }

        private void OnDisable()
        {
            OnNodeCountUpdatedStatic -= OnNodeCountUpdated;
            foreach (var chainController in chainControllers)
            {
                //  chainController.OnNodeListUpdated -= OnNodesUpdated;
                //chainController.OnLeaderChanged -= OnLeaderChanged;
            }
        }

        public void Initialize()
        {
            var targetGroup = FindObjectOfType<CinemachineTargetGroup>();
            targetGroup?.AddMember(NodeToFollow.transform, 1, 1);
            var lenght = chainControllers.Length;
            var count = chainControllerData.InitialNodeCount;
            var remaining = count % lenght;
            int nodePerController= count / lenght;
            for (int i = 0; i < lenght; i++)
            {
               // chainControllers[i].OnNodeListUpdated += OnNodesUpdated;
                //chainController.OnLeaderChanged += OnLeaderChanged;
                chainControllers[i].SetChainController(chainControllerData);
                chainControllers[i].ChainControllerData.InitialNodeCount = nodePerController + i<remaining?1:0;
                chainControllers[i].Initialize();
            }
            ReorderChainControllers();
            StartCoroutine(CheckMaxDistance());
        }

        IEnumerator CheckMaxDistance()
        {
            while (true)
            {
                yield return new WaitForSecondsRealtime(checkInterval);
                if (checkMaxDistance)
                {
                    if (NodeToFollow)
                    {
                        foreach (var chainController in chainControllers)
                        {
                            foreach (var node in chainController.GetNodes())
                            {
                                if (Vector3.Distance(NodeToFollow.transform.position, node.transform.position) > maxDistance)
                                    node.transform.position = NodeToFollow.transform.position + maxDistanceOffset;
                            }
                        }
                    }
                }
            }
        }

        void ReorderChainControllers()
        {

            var nodes = new List<ChainNode>();
            foreach (var controller in chainControllers)
            {
                nodes.AddRange(controller.Nodes);
                nodes.ForEach(n => n.DestroyNode());
            }
            var length = chainControllers.Length;

            for (int i = 0; i < nodes.Count; i++)
            {
                chainControllers[i % length].AddNode(nodes[i],false);
                nodes[i].transform.position = NodeToFollow.transform.position;
                nodes[i].OnNodeAdded?.Invoke();
            }

            foreach (var controller in chainControllers)
            {
                controller.SetNodeParents();
                if(controller.GetChainLeader()!=null)
                    controller.GetChainLeader().SetParent(NodeToFollow);
            }
            NodeToFollow.transform.parent = transform;
            OnNodesUpdated();
        }

        ChainController GetLowestController()
        {
            ChainController lowestChainController = null;
            var count = int.MaxValue;
            foreach (var chainController in chainControllers)
            {
                if(chainController.NodeCount<=count)
                {
                    lowestChainController = chainController;
                    count = chainController.NodeCount;
                }
            }
            return lowestChainController;
        }

        ChainController GetHighestController()
        {
            ChainController highestChainController = null;
            var count = int.MinValue;
            foreach (var chainController in chainControllers)
            {
                if (chainController.NodeCount > count)
                {
                    highestChainController = chainController;
                    count = chainController.NodeCount;
                }
            }
            return highestChainController;
        }

        void OnNodesUpdated()
        {
            lastUpdatedTime = Time.realtimeSinceStartup;
            OnNodeCountUpdated?.Invoke(TotalNodeCount);
            OnNodeCountUpdatedStatic?.Invoke(TotalNodeCount);
            if (TotalNodeCount > 0 && !GameStateManager.Instance.BonusActive && !GameStateManager.Instance.InitialStateActive)
                ToggleManager.Instance.SetToggle(TotalNodeCount / 10);
        }

        IEnumerator SetParent(ChainNode node)
        {
            yield return new WaitForSecondsRealtime(.1f);
            node.SetParent(NodeToFollow);
        }

        [Button("Add Nodes")]
        public void AddNodes(int value)
        {
            if (chainControllers.Length == 0) return;
            //var targetNodeCount = TotalNodeCount + value;
            //int targetNodePerController = targetNodeCount / chainControllers.Length;
            //var remaing = targetNodeCount - targetNodePerController * chainControllers.Length;

            //for (int i = 0; i < value; i++)
            //{
            //    chainControllers[i% chainControllers.Length].CreateNode(chainControllerData.PrefabNode,false);
            //}
            //foreach (var controller in chainControllers)
            //{
            //    var addAmount= targetNodePerController-controller.NodeCount;

            //    if (addAmount > 0)
            //        controller.CreateNodes(chainControllerData.PrefabNode, addAmount);
            //}
            //for (int i = 0; i < remaing; i++)
            //{
            //    chainControllers[i].CreateNodes(chainControllerData.PrefabNode,1);
            //}

            for (int i = 0; i < value; i++)
            {
                chainControllers[i % chainControllers.Length].CreateNode(chainControllerData.PrefabNode, false);
            }
            ReorderChainControllers();
        }

        [Button("Remove Nodes")]
        public void RemoveNodes(int count,bool removeLast=true, bool callOnNodeRemoved = true)
        {
            if (chainControllers.Length == 0) return;
            count = Mathf.Min(count, TotalNodeCount);
            for (int i = 0; i < count; i++)
            {
                var highestController = GetHighestController();
                ChainNode node;
                if (removeLast)
                    node = highestController.LastNode;
                else
                    node = highestController.GetChainLeader();
                node?.DestroyNode(callOnNodeRemoved);
            }
            ReorderChainControllers();
        }

        public void RemoveNodeList(List<ChainNode> nodes)
        {
            if (chainControllers.Length == 0) return;
            for (int i = 0; i < nodes.Count; i++)
            {
                nodes[i]?.DestroyNode();
            }
            ReorderChainControllers();
        }

        public void RemoveClosestNodes(ChainNode mainNode, int count, bool removeLast = true)
        {
            if (chainControllers.Length == 0) return;
            if (!mainNode.NodeController) return;
            var index = mainNode.GetIndex();
            if (index<0) return;
            count = Mathf.Min(count, TotalNodeCount);
            int counter = 0;
            
            for (int i = 0; i < int.MaxValue; i++)
            {
                    for (int j = 0; j < 2; j++)
                    {

                    counter++;
                    if (counter < count)
                        break;
                    }
                if (counter < count)
                    break;
                var highestController = GetHighestController();
                ChainNode node;
                if (removeLast)
                    node = highestController.LastNode;
                else
                    node = highestController.GetChainLeader();
                node?.DestroyNode();
            }
            ReorderChainControllers();
        }



        List<ChainNode> GetNodesAround(ChainNode mainNode, int count)
        {
            if (chainControllers.Length == 0) return null;
            if (!mainNode.NodeController) return null;
            var index = mainNode.GetIndex();
            if (index < 0) return null;
            List<ChainNode> nodesAround=new List<ChainNode>();
            count = Mathf.Min(count, TotalNodeCount);
            int counter = 0;
            int currentIndex = 0;
            for (int i = 0; i < int.MaxValue; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    currentIndex = index + (int)Math.Pow(-1, j);

                    counter++;
                    if (counter < count)
                        break;
                }
                if (counter < count)
                    break;
            }
            return nodesAround;
        }

        public List<ChainNode> GetNodes()
        {
            List<ChainNode> nodes = new List<ChainNode>();
            foreach (var controller in chainControllers)
            {
                nodes.AddRange(controller.GetNodes());
            }
            return nodes;
        }

        public void SetNodeCount(int nodeCount)
        {
            if (nodeCount < TotalNodeCount)
                RemoveNodes(TotalNodeCount- nodeCount,false,false);
            if (nodeCount > TotalNodeCount)
                AddNodes(nodeCount-TotalNodeCount);
        }
    }


}
