﻿using System.Collections.Generic;
using UnityEngine;

namespace GroupMovement.ChainMovementSystem
{
    public class GroupNodeRemoverOnEnterWithDelay : MonoBehaviour
    {
        [ReadOnly] [SerializeField] List<ChainNode> nodes = new List<ChainNode>();
        [ReadOnly] [SerializeField] bool isRemovalStarted = false;
        [SerializeField] float removeTime = 1;
        [ReadOnly] [SerializeField] GroupChainController group;

        public void OnTriggerEnter(Collider other)
        {
            var node = other.GetComponent<ChainNode>();
            if (!isRemovalStarted)
                group = node.NodeController?.GetComponentInParent<GroupChainController>();
            if (group == null) return;
            nodes.Add(node);
            if (!isRemovalStarted)
            {
                isRemovalStarted = true;
                Invoke(nameof(RemoveNodes), removeTime);
            }
        }

        void RemoveNodes()
        {
            group.RemoveNodeList(nodes);
            nodes.Clear();
            group = null;
            isRemovalStarted = false;
        }
    }



}


