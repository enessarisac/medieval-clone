﻿using System;
using UnityEngine;

namespace GroupMovement.ChainMovementSystem
{
    [Serializable]
    public class NodeData
    {
        public bool IsPhysicsMovement;
        public bool FollowLocal;
        public bool AlignX;
        public float CurrentAlignmentSpeedX;
        public bool AlignY;
        public float CurrentAlignmentSpeedY;
        public bool AlignZ;
        public float CurrentAlignmentSpeedZ;
        public Vector3 Offset;
        public Vector3 IgnoreDistance = 0.1f*Vector3.one;
        public float MinDistanceMultiplier = 0.5f;
        public float MaxDistanceMultiplier = 3f;
        public bool Removable=true;
    }
}