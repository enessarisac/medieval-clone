﻿using System;
using UnityEngine;

namespace GroupMovement.ChainMovementSystem
{
    [Serializable]
    public class TransformNodeAlignerLocal : NodeAligner
    {
        public TransformNodeAlignerLocal(ChainNode node) : base(node)
        {
        }

        public override void AlignWithX(Transform target, float alignmentSpeed)
        {

        }

        public override void AlignWithY(Transform target, float alignmentSpeed)
        {
        }

        public override void AlignWithZ(Transform target, float alignmentSpeed)
        {
        }
    }
}
