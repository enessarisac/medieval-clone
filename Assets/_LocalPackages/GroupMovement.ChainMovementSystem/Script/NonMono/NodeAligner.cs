﻿using System;
using UnityEngine;

namespace GroupMovement.ChainMovementSystem
{
    [Serializable]
    public abstract class NodeAligner
    {
        //public float IgnoreDistance = 0.1f;
        //public float MinDistanceMultiplier = 0.5f;
        //public float MaxDistanceMultiplier = 3f;
        ChainNode node;
        protected NodeData NodeData { get { return node.NodeData; } }
        protected Transform nodeTransform;

        public NodeAligner(ChainNode node)
        {
            this.node = node;
            nodeTransform = node.transform;
        }

        public abstract void AlignWithX(Transform target, float alignmentSpeed);
        public abstract void AlignWithY(Transform target, float alignmentSpeed);
        public abstract void AlignWithZ(Transform target, float alignmentSpeed);
    }
}
