using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StatusSystem
{

    [CreateAssetMenu(fileName = "StunnedStatus", menuName = "StatusSystem/Stunned Status")]
    public class Stunned : Status
    {
        protected Transform transform;
        protected GameObject gameObject;

        //MobileInputListener mobileInputListener;
        //KeyBoardInputListener keyBoardInputListener;
        //BotStateMachine botStateMachine;

        public override void ActivateStatus()
        {
            //StunRobot();
        }

        public override void ReActivateStatus()
        {
            //StunRobot();
        }

        public override void DeActivateStatus()
        {
            ReleaseRobot();
        }

        public override void Initialize(StatusController owner)
        {
            Debug.LogError("Stunned status is not implemented");
            return;

            base.Initialize(owner);

            //transform = owner.transform;
            //gameObject = owner.gameObject;

            //agentController = gameObject.GetComponent<RobotController>();
            //botStateMachine = gameObject.GetComponent<BotStateMachine>();
            
        }

        public void StunRobot()
        {
            //if (botStateMachine)
            //    botStateMachine.enabled = false;
            //agentController.ReleaseWeaponButton();
            //agentController.DisableMovement();
        }

        public void ReleaseRobot()
        {
            //if (botStateMachine)
            //    botStateMachine.enabled = true;
            //agentController.EnableMovement();
        }
    }
}