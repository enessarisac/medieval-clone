using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    [SerializeField] float pitchRange = 0;
    AudioSource audioSource;
    string gameSoundParameter = "GameVolume";

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound()
    {
        float value;
        audioSource.outputAudioMixerGroup.audioMixer.GetFloat(gameSoundParameter, out value);
        if (value == -80) return;

        if (pitchRange != 0) audioSource.pitch = 1 + UnityEngine.Random.Range(-pitchRange, pitchRange);
        AudioSource.PlayClipAtPoint(audioSource.clip, transform.position);
    }
}
