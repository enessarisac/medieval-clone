using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Indicator : MonoBehaviour
{
    [SerializeField] Slider indicatorSlider;
    [SerializeField] bool isIndicatorOn;
    [SerializeField] int minValue = 5;
    [SerializeField] int maxValue;
    [SerializeField] float updateValue;
    [SerializeField] TMP_Text startValueLeft;
    [SerializeField] TMP_Text midValueLeft;
    [SerializeField] TMP_Text midValueRight;
    [SerializeField] TMP_Text endValueRight;
    [SerializeField] TMP_Text highestValue;
    public Action<int> OnIndicatorStoped;
    int updatgeSign = 1;
    public bool IsIndicatorOn { get{ return isIndicatorOn; } }

    void Awake()
    {
        SetValue();
    }

    void SetValue()
    {
        startValueLeft?.SetText((maxValue * .25f).ToString());
        midValueLeft?.SetText((maxValue * .5f).ToString());
        midValueRight?.SetText((maxValue * .5f).ToString());
        highestValue?.SetText(maxValue.ToString());
        endValueRight?.SetText((maxValue * .25f).ToString());
    }

    void FixedUpdate()
    {
        if (isIndicatorOn)
            UpdateIndicator();
    }

    public void StartIndicator()
    {
        indicatorSlider.value = 0;
        isIndicatorOn = true;
    }
    
    public void StopIndicator()
    {
        isIndicatorOn = false;
        OnIndicatorStoped?.Invoke(GetIndicatorValue());
    }

    public void SetIndicatorSpeed(float speed)
    {
        updateValue = speed;
    }

    void UpdateIndicator()
    {
        indicatorSlider.value += updateValue * updatgeSign;
        if (indicatorSlider.value >= indicatorSlider.maxValue)
        {
            indicatorSlider.value = indicatorSlider.maxValue;
            updatgeSign = -1;
        }

        if (indicatorSlider.value <= indicatorSlider.minValue)
        {
            indicatorSlider.value = indicatorSlider.minValue;
            updatgeSign = 1;
        }
    }

    int GetIndicatorValue()
    {
        var slidervalue = indicatorSlider.value/ indicatorSlider.maxValue;
        if (slidervalue > .5)
            slidervalue = 1 - slidervalue;
        return Mathf.Max(minValue,(int)(slidervalue * maxValue * 2));
    }
}