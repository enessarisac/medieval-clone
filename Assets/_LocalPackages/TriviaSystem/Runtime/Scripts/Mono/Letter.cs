﻿using UnityEngine;
using UnityEngine.Events;

namespace TriviaSystem
{
    public class Letter : MonoBehaviour
    {

        [ReadOnly] [SerializeField] public char Character;
        [SerializeField] UnityEvent<char> onCharacterSet;


        public void SetCharacter(char character)
        {
            if (' ' == character) return;
            this.Character = char.ToUpper(character);
            onCharacterSet?.Invoke(character);
        }

        public bool MatchAny(string word) { return MatchAny(word.ToCharArray()); }
        public bool MatchAny(char[] word)
        {
            for (int i = 0; i < word.Length; i++)
            {
                if (Character.CompareWithoutCaseSensitivity(word[i]))
                    return true;
            }
            return false;
        }

    }
}