using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace TriviaSystem
{
    public class TriviaLevelManager : MonoBehaviour
    {

        [SerializeField] TriviaSubject subject;
        [SerializeField] TriviaWordData[] wordList;

        private void OnValidate()
        {
            SetLegitWords();
        }

        void SetLegitWords()
        {
            List<TriviaWordData> legitWords = new List<TriviaWordData>();
            foreach (var data in wordList)
            {
                if (subject.IsSame(data.Subject))
                    legitWords.Add(data);
            }
            wordList = legitWords.ToArray();
        }

        public bool IsLegit(string checkWord) 
        {
            for (int i = 0; i < wordList.Length; i++)
            {
                var word = wordList[i].Word;
                if (word.StartsWith(checkWord))
                    return true;
            }
            return false;
        }
        public bool IsWholeWord(string word) { return GetNextChars(word).Length==0; }

        public List<char> GetUniqueLevelLetters()
        {
            var uniqueLetters = new List<char>();
            foreach (TriviaWordData data in wordList)
            {
                uniqueLetters.AddRange(data.Word.ToCharArray());
            }
            return uniqueLetters.Distinct().ToList();
        }

        public char[] GetNextChars(LetterManager letterManager)
        {
            var currentWord = letterManager.GetCurrentWord;
            List<string> availableWords = GetAvailableWords(currentWord);
            for (int i = 0; i < letterManager.CollectedWords.Count; i++)
            {
                var collectedWord = letterManager.CollectedWords[i];
                var foundWord = availableWords.Find(word => word.ToLower() == collectedWord.ToLower());
                if (foundWord != null)
                    availableWords.Remove(foundWord);
            }
            List<char> nextChars = GetAvailableChars(currentWord.Length, availableWords);
            return nextChars.Distinct().ToArray();
        }
        public char[] GetNextChars(string currentWord)
        {
            List<string> availableWords = GetAvailableWords(currentWord);
            List<char> nextChars = GetAvailableChars(currentWord.Length, availableWords);
            return nextChars.Distinct().ToArray();
        }

        List<char> GetAvailableChars(int index, List<string> availableWords)
        {
            List<char> nextChars = new List<char>();
            for (int i = 0; i < availableWords.Count; i++)
            {
                nextChars.Add(availableWords[i][index]);
            }
            return nextChars;
        }

        private List<string> GetAvailableWords(string currentWord )
        {
            List<string> availableWords = new List<string>();
            for (int i = 0; i < wordList.Length; i++)
            {
                var word = wordList[i].Word;
                if (string.IsNullOrEmpty(currentWord) || (word.Length > currentWord.Length && word.ToLower().StartsWith(currentWord.ToLower())))
                    availableWords.Add(wordList[i].Word);
            }
            return availableWords;
        }

        public void SetAsCurrentSubject()
        {
            TriviaSubject.OnSubjectChanged?.Invoke(subject);
        }
    }

}