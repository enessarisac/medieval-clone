using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiater : MonoBehaviour
{
    [SerializeField] GameObject prefab;
    [SerializeField] float interval;

    void Start()
    {
        InvokeRepeating(nameof(Instantiate), interval, interval);    
    }

    // Update is called once per frame
    void Instantiate()
    {
        if(prefab)
            Instantiate(prefab, transform.position, transform.rotation);
    }
}
