using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameLevelSystem;
using GroupMovement.ChainMovementSystem;

public class GameStateManager : MonoBehaviourSingleton<GameStateManager>
{

    public static Action OnGameLevelLoaded;
    public static Action OnGameLevelUnloaded;
    public bool InitialStateActive = true;
    public bool LevelWin = false;
    public bool BonusActive = false;
    public bool EndScreenActive = false;

    private void OnEnable() {
        OnGameLevelLoaded += ResetVariables;
    }
    private void OnDisable() {
        OnGameLevelLoaded -= ResetVariables;
    }

    public void InvokeRetryButtonClicked()
    {
        LevelManager.Instance.ReloadLevel();
    }

    public void InvokeNextLevelButtonClicked()
    {

    }

    void ResetVariables()
    {
        BonusActive = false;
        LevelWin = false;
        InitialStateActive = true;
        EndScreenActive = false;
    }

    public void SetInitialState(bool value)
    {
        InitialStateActive = value;
    }
    
}
