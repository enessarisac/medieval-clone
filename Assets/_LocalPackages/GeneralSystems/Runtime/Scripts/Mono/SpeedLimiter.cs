using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class SpeedLimiter : MonoBehaviour
{
    [Toggle("Enabled")]
    public FloatToggleable LimitX = new FloatToggleable();
    [Toggle("Enabled")]
    public FloatToggleable LimitY = new FloatToggleable();
    [Toggle("Enabled")]
    public FloatToggleable LimitZ = new FloatToggleable();

    Rigidbody rb;
    private void Awake() 
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate() 
    {
        if(LimitX.Enabled && rb.velocity.x > LimitX.Value)
        {
            var targetVel = rb.velocity;
            targetVel.x = LimitX.Value;
            rb.velocity = targetVel;
        }
        else if(LimitY.Enabled && rb.velocity.y > LimitY.Value)
        {
            var targetVel = rb.velocity;
            targetVel.y = LimitY.Value;
            rb.velocity = targetVel;
        }
        else if(LimitZ.Enabled && (rb.velocity.z > LimitZ.Value || rb.velocity.z < LimitZ.Value))
        {
            var targetVel = rb.velocity;
            targetVel.z = LimitZ.Value;
            rb.velocity = targetVel;
        }
    }
}
