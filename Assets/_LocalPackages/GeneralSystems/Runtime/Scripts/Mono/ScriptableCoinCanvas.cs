﻿using ScriptableSystem;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableCoinCanvas", menuName = "ScriptableManager/ScriptableCoinCanvas")]
public class ScriptableCoinCanvas : ScriptableManager<ImageDuplicateAndMoveFeedback>
{
    public void CreateCoins(int amount)
    {
        Instance.CreateImages(amount);
    }
}
