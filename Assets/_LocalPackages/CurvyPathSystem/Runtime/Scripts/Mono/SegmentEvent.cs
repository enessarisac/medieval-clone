using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FluffyUnderware.Curvy.Controllers;
using ScriptableSystem;
using UnityEngine;
using UnityEngine.Events;

public class SegmentEvent : MonoBehaviour
{
    [SerializeField] List<EventTagPair> eventTagPairList;

    void Awake()
    {
        if(eventTagPairList.Count == 0) return;

        eventTagPairList = new List<EventTagPair>(eventTagPairList.GroupBy(o => o.TargetTag.Value).Select(g => g.First()));
    }

    public void OnControlPointReached(SplineController splineController)
    {
        foreach(var pair in eventTagPairList)
        {
            if(string.IsNullOrEmpty(pair.TargetTag.Value))
            {
                pair.OnPointReached?.Invoke();
                pair.OnPointReachedObject?.Invoke(splineController.gameObject);
                break;
            }
            else if(splineController.transform.GetChild(0).CompareTag(pair.TargetTag.Value))
            {
                pair.OnPointReached?.Invoke();
                pair.OnPointReachedObject?.Invoke(splineController.gameObject);
                break;
            }
        }
    }

    [Serializable]
    private class EventTagPair
    {
        public StringReference TargetTag;
        public UnityEvent OnPointReached;
        public UnityEvent<GameObject> OnPointReachedObject;
    }
}


