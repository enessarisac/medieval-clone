using System.Collections;
using System.Collections.Generic;
using ScriptableSystem;
using Sirenix.OdinInspector;
using UnityEngine;

public class SplineBounceBack : MonoBehaviour
{
    [SerializeField] ScriptableFloat currentDistance;
    [SerializeField] FloatReference moveBackAmount;
    [SerializeField] FloatReference moveBackSpeed;
    [SerializeField] Cooldown cooldown=new Cooldown(1);
    Coroutine coroutine;
    float startDistance;

    [Button]
    public void BounceBack()
    {
        BounceBack(moveBackSpeed.Value);
    }

    [Button]
    public void BounceBack(float backSpeed)
    {
        if (!cooldown.IsCooldownFinished)
            return;
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
            currentDistance.UpdateValue(startDistance);
        }
        coroutine = StartCoroutine(IEBounceBack(backSpeed));
    }


    IEnumerator IEBounceBack(float backSpeed)
    {
        startDistance = currentDistance.GetValue();
        cooldown.UpdateCooldown();
        while (startDistance -moveBackAmount.Value < currentDistance.GetValue())
        {
            //Debug.Log(currentDistance.Value);
            yield return null;
            var newValue = currentDistance.GetValue() - backSpeed * Time.deltaTime* (cooldown.LeftTime+1);
            currentDistance.UpdateValue(newValue);
            if (cooldown.IsCooldownFinished)
                break;
        }
    }
}
