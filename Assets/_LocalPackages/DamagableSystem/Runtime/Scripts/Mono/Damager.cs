﻿using ScriptableSystem;
using UnityEngine;
using UnityEngine.Events;

namespace DamagableSystem
{
    public class Damager : MonoBehaviour
    {
        [SerializeField] float damage;
        [SerializeField] UnityEvent onDamage;

        public void ApplyDamage(GameObject go)
        {
            var damagable = go.GetComponent<IDamagable>();
            if (damagable != null)
            {
                    damagable.ApplyDamage(damage);
                    onDamage?.Invoke();
            }
        }
    }
}