﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace SequenceSystem
{
    [Serializable]
    public class SequenceData
    {
        public SequenceTrigger SequenceTrigger;
        [SerializeField] UnityEvent sequenceStartAction;
        [SerializeField] UnityEvent sequenceUpdateAction;
        [SerializeField] UnityEvent sequenceEndAction;

        public void OnStart() { SequenceTrigger.OnStart(); sequenceStartAction?.Invoke(); }
        public void OnUpdate() { SequenceTrigger.OnUpdate(); sequenceUpdateAction?.Invoke(); }
        public void OnEnd() { SequenceTrigger.OnEnd(); sequenceEndAction?.Invoke(); }
    }

}