﻿# if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using Sirenix.OdinInspector;
using System.Linq;

namespace PhysicsAnimationSystem
{
    
    public class PhysicsRecorder : MonoBehaviour
    {
        [SerializeField] AnimationClip clip;
        [SerializeField] bool recordOnStart;

        GameObjectRecorder recorder;

        [ReadOnly] [SerializeField] List<Collider> addedColliders;
        [ReadOnly] [SerializeField] List<Rigidbody> addedRBs;

        void Start()
        {
            if (!recordOnStart) return;
            StartRecording();
        }

        public void StartRecording()
        {
            // Create recorder and record the script GameObject.
            recorder = new GameObjectRecorder(gameObject);

            // Bind all the Transforms on the GameObject and all its children.
            recorder.BindComponentsOfType<Transform>(gameObject, true);
        }

        void LateUpdate()
        {
            if (clip == null)
                return;
         //   if (!recorder || !recorder.isRecording) return;

            // Take a snapshot and record all the bindings values for this frame.
            recorder.TakeSnapshot(Time.deltaTime);
        }

        void OnDisable()
        {
            if (clip == null)
                return;

            StopRecording();
        }

        private void StopRecording()
        {
            if (recorder.isRecording)
            {
                recorder.SaveToClip(clip);
            }
        }

        [Button]
        public void AddCollidersAndRBs()
        {
            var renderers = GetComponentsInChildren<MeshRenderer>();

            for (int i = 0; i < renderers.Length; i++)
            {
                var go= renderers[i].gameObject;
                var col = go.GetComponent<Collider>();
                var rb = go.GetComponent<Rigidbody>();

                if(col.IsNull())
                {
                    col = go.AddComponent<MeshCollider>();
                    ((MeshCollider)col).convex = true;
                    addedColliders.Add(col);
                }
                addedColliders = addedColliders.Distinct().ToList();
                if (rb.IsNull())
                {
                    rb = go.AddComponent<Rigidbody>();
                    addedRBs.Add(rb);
                }
                addedRBs = addedRBs.Distinct().ToList();
            }
        }


        [Button]
        public void RemoveAddedCollidersAndRBs()
        {
            foreach (var col in addedColliders)
            {
                if(col)
                    DestroyImmediate(col);
            }
            addedColliders.Clear();
            foreach (var rb in addedRBs)
            {
                if(rb)
                    DestroyImmediate(rb);
            }
            addedRBs.Clear();

        }

    }
}
# endif