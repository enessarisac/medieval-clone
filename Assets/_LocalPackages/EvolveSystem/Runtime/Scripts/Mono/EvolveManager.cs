using System;
using System.Collections;
using ScriptableSystem;
using UnityEngine;
using UnityEngine.Events;

namespace EvolveSystem
{
    public class EvolveManager : MonoBehaviourSingleton<EvolveManager>
    {
        public ScriptableFloat CurrentMoveSpeed;
        [SerializeField] ScriptableFloat InitialMoveSpeed;
        [SerializeField] EvolveLevelData[] evolveLevelDatas;
        [ReadOnly] [SerializeField] EvolveLevelData currentEvolveLevelData;
        public Animator Animator;
        [SerializeField] String animatorKey;
        [SerializeField] String evolveKey = "OnEvolve";
        [SerializeField] int currentLevel=0;
        public int MaxLevel{get{return evolveLevelDatas.Length;}}
        [Header("Events")]
        [SerializeField] UnityEvent onEvolveUp;
        [SerializeField] UnityEvent onEvolveDown;
        public static Action OnEvolveUp;
        public static Action OnEvolveDown;
        void Awake()
        {
            foreach (var data in evolveLevelDatas)
            {
                DisableEvolveLevel(data);
            }
            SetCurrentEvolveLevel();
        }

        [ContextMenu("EvolveUp")]
        public void EvolveUp()
        {
            SetLevel(currentLevel + 1);
        }

        [ContextMenu("EvolveDown")]
        public void EvolveDown()
        {
            SetLevel(currentLevel - 1);
        }

        public void SetLevel(int level)
        {
            if (level >= 0&& level <= evolveLevelDatas.Length - 1)
            {
                var oldLevel = currentLevel;
                if (currentLevel > level)
                {
                    onEvolveDown?.Invoke();
                    OnEvolveDown?.Invoke();
                }
                else if (currentLevel < level)
                {
                    onEvolveUp?.Invoke();
                    OnEvolveUp?.Invoke();
                }
                currentLevel = level;
                //Invoke(nameof(SetCurrentEvolveLevel), 0.5f);
                if(level != oldLevel)
                {
                    SetCurrentEvolveLevel();
                    Animator.SetTrigger(evolveKey);
                }
            }
        }

        public void SetCurrentEvolveLevel()
        {
            if (currentEvolveLevelData != null)
                DisableEvolveLevel(currentEvolveLevelData);
            currentEvolveLevelData = evolveLevelDatas[currentLevel];
            if (currentEvolveLevelData == null) return;
            currentEvolveLevelData.ToggleObjects(true);
            Animator = currentEvolveLevelData.Animator;
            Debug.Log("index " + currentLevel);
            if (Animator)
            {
                Animator.SetInteger(animatorKey, currentLevel);
                
            }
        }

        void DisableEvolveLevel(EvolveLevelData evolveLevelData)
        {
            evolveLevelData.ToggleObjects(false);
        }

        public int GetCurrentLevel()
        {
            return currentLevel;
        }

        public void SetEvolveWithDelay(float delay)
        {
            Invoke(nameof(SetWalk), delay);

        }

        void SetWalk()
        {
            SetCurrentEvolveLevel();
            Animator.ResetTrigger("Hit");
            StartCoroutine(IEIncreaseSpeed());
        }

        IEnumerator IEIncreaseSpeed()
        {
            while (CurrentMoveSpeed.GetValue() < InitialMoveSpeed.GetValue())
            {
                CurrentMoveSpeed.UpdateValue(CurrentMoveSpeed.GetValue() + 0.1f);
                yield return new WaitForSeconds(0.005f);
            }

            CurrentMoveSpeed.UpdateValue(InitialMoveSpeed.Value);
        }
    }
}

