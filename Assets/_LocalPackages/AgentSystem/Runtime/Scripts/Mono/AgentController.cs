using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AgentSystem
{
    using StatusSystem;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class AgentController : MonoBehaviour
    {
        public bool DebugBool;
        bool isBelongsToPlayer;
        public Agent Agent { get; private set; }
        public static Action<Agent> OnAgentControllerCreated;
        public float AgentVelocity { get { return agentMovement.GetVelocity(); } }

        [SerializeField] InputListener inputListener;
        [SerializeField]
        List<InputListener> inputListeners = new List<InputListener>();
        AgentMovement agentMovement;
        AgentRotate agentRotate;
        AgentPhysicController agentPhysicController;
        StatusController statusController;


        protected virtual void Awake()
        {
            InitializeRobotComponents();
        }

        private void Start()
        {
            //if (isBelongsToPlayer)
            //{
            //    if (AudioListenerController.Instance == null)
            //    {
            //        var audioListenerPrefab = Resources.Load<AudioListenerController>("AudioListener");
            //        var audioListener = Instantiate(audioListenerPrefab).GetComponent<AudioListenerController>();
            //        audioListener.SetTarget(transform);
            //    }
            //    else
            //    {
            //        AudioListenerController.Instance.SetTarget(transform);
            //    }
            //    CharacterAttack.CreateTargetLock();
            //}
            Agent.SendCurrentAgentAttributes();
        }



        #region Initialize

        protected virtual void InitializeRobotComponents()
        {
            Agent = GetComponent<Agent>();
            agentMovement = gameObject.AddComponent<AgentMovement>();
            agentRotate = gameObject.AddComponent<AgentRotate>();
            agentPhysicController = new AgentPhysicController(gameObject);
            statusController = gameObject.AddComponent<StatusController>();
            Agent.SetBaseRobotAttributes();
        }

        #endregion Initialize

        

        private void OnEnable()
        {
            if (inputListener != null)
            {
                AddInputListenerActions(inputListener);
                inputListeners.Add(inputListener);
            }
        }

        private void OnDisable()
        {
            RemoveListenerActions();
        }

        

        #region Listener
        public void RemoveListenerActions()
        {
            for (int i = 0; i < inputListeners.Count; ++i)
            {
                if (inputListeners[i] == null) continue;
                RemoveInputListenerActions(inputListeners[i]);
                Destroy(inputListeners[i]);
            }
            inputListeners.RemoveAll(i => i == null);
            //StopMoving();
        }

        public void SetInputListener(InputListener inputListener, bool additive = false)
        {
            if (!additive)
            {
                RemoveListenerActions();
                inputListeners.Clear();
            }
            this.inputListener = inputListener;

            inputListeners.Add(inputListener);

            AddInputListenerActions(inputListener);
        }

        [ContextMenu("SetCurrentInputListenerActions")]
        void SetCurrentInputListenerActions()
        {
            AddInputListenerActions(inputListener);
            inputListeners.Add(inputListener);
        }

        private void AddInputListenerActions(InputListener inputListener)
        {
            inputListener.OnMoveHold += Move;
            inputListener.OnMoveReleased += StopMove;
        }

        private void RemoveInputListenerActions(InputListener inputListener)
        {
            inputListener.OnMoveHold -= Move;
            inputListener.OnMoveReleased?.Invoke();
            inputListener.OnMoveReleased -= StopMove;
        }
        #endregion Listener

        #region Movement 
        public void DisableMovement() //ayri class yazilacak
        {
            inputListeners.ForEach(inputListener => inputListener.enabled = false);
            Debug.Log("Count: " + inputListeners.Count);
        }

        public void EnableMovement()
        {
            inputListeners.ForEach(inputListener => inputListener.enabled = true);
        }

        void Move(Vector2 inputVector)
        {
            //rotate
            agentMovement.MoveCharacter(inputVector);
            agentRotate.Rotate(inputVector);
        }

        void StopMove()
        {
            agentMovement.MoveCharacter(Vector2.zero);
        }

        #endregion Movement

        public void Rotate(Vector2 inputVector)
        {
            agentRotate.Rotate(inputVector);
        }

    }

}