using UnityEngine;

namespace InterfaceSystem
{
    public interface ITransform
    {
        Transform Transform { get; }

        void SetTransform(Transform transform);

    }
}