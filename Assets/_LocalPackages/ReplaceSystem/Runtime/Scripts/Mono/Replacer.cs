﻿using System.Collections.Generic;
using UnityEngine;

namespace ReplaceSystem
{
    [AddComponentMenu("LocalPackages/ReplaceSystem/Replacer")]
    public class Replacer : MonoBehaviour
    {
        [SerializeField] Cooldown replaceCooldown;
        [SerializeField] List<ReplacableData> replacableDatas;



        public void TryReplace(GameObject go)
        {
            if (!replaceCooldown.IsCooldownFinished) return;
            var replaceController = go.GetComponentInChildren<ReplaceController>();
            if (replaceController)
            {
                var data = replacableDatas.Find(rd => rd.Replaced == replaceController.ReplacableObject);
                if (!data.IsNull())
                {
                    replaceController.ReplaceObject(data.Replacement);
                    replaceCooldown.UpdateCooldown();
                }
            }
        }
    }
}