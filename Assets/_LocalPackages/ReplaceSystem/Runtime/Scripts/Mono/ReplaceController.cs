using System;
using System.Collections.Generic;
using UnityEngine;

namespace ReplaceSystem
{
    [AddComponentMenu("LocalPackages/ReplaceSystem/ReplaceController")]
    public class ReplaceController : MonoBehaviour
    {
        public ReplacableObject ReplacableObject;
        [SerializeField]Cooldown replacementCooldown;

        private void Awake()
        {
            replacementCooldown.UpdateCooldown();
        }

        public virtual void ReplaceObject(ReplacableObject replacement)
        {
            if (!replacementCooldown.IsCooldownFinished) return;
            var replacable=Instantiate(replacement.Replacable,transform.parent).transform;
            replacable.localPosition = transform.localPosition;
            replacable.localRotation = transform.localRotation;
            Destroy(gameObject);
        }
    }

}
