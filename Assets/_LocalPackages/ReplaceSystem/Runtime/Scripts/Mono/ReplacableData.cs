﻿using UnityEngine;

namespace ReplaceSystem
{
    [CreateAssetMenu(menuName = "ReplaceSystem/ScriptableData/ReplacableData")]
    public class ReplacableData : ScriptableObject
    {
        public ReplacableObject Replaced;
        public ReplacableObject Replacement;
    }

}
