using System.Collections;
using System.Collections.Generic;
using GameLevelSystem;
using ScriptableSystem;
using UnityEngine;

public class SwerveMovement : MonoBehaviour
{
    public MovementType MovementType = MovementType.PathMovement;
    [SerializeField] float swerveSpeed = 1;
    [SerializeField] float maxSwerveAmount = 2f;
    [SerializeField] bool stopRbOnTouchRelease = true;
    [SerializeField] [ReadOnly] float swerveAmountDebug;
    Rigidbody rb;
    bool CanMoveLeft = true;
    bool CanMoveRight = true;
    bool swerveEnabled = true;

    private void Awake() {
        rb = GetComponent<Rigidbody>();
    }
    private void OnEnable() 
    {
        if(stopRbOnTouchRelease)
            SwerveInputManager.OnTouchReleased += StopPlayer;
    }
    private void OnDisable() 
    {
        if(stopRbOnTouchRelease)
            SwerveInputManager.OnTouchReleased -= StopPlayer;
    }

    private void Update() 
    {
        if(!swerveEnabled) return;

        float swerveAmount = swerveSpeed * SwerveInputManager.Instance.MoveFactorX * Time.deltaTime * 10;
        swerveAmountDebug = swerveAmount;
        swerveAmount = Mathf.Clamp(swerveAmount, -maxSwerveAmount, maxSwerveAmount);
        var position = transform.localPosition;

        if(Mathf.Approximately(swerveAmount, 0) && !Mathf.Approximately(rb.velocity.x, 0) && MovementType == MovementType.PathMovement)
        {
            Vector3 targetVel = new Vector3(-rb.velocity.x, 0, -rb.velocity.z);
            rb.AddForce(targetVel, ForceMode.Impulse);
        }
        

        if(position.x > SwerveInputManager.Instance.HalfLaneSize)
        {
            position.x = SwerveInputManager.Instance.HalfLaneSize;
            transform.localPosition = position;
            StopPlayerX();
            CanMoveRight = false;
        }
        else if(position.x < -SwerveInputManager.Instance.HalfLaneSize)
        {
            position.x = -SwerveInputManager.Instance.HalfLaneSize;
            transform.localPosition = position;
            StopPlayerX();
            CanMoveLeft = false;
        }
        else
        {
            if(position.x < SwerveInputManager.Instance.HalfLaneSize - 0.1f && !Mathf.Approximately(position.x, SwerveInputManager.Instance.HalfLaneSize))
            {
                    CanMoveRight = true;
            }
            if(position.x > -SwerveInputManager.Instance.HalfLaneSize + 0.1f && !Mathf.Approximately(position.x, -SwerveInputManager.Instance.HalfLaneSize))
            {
                CanMoveLeft = true;
            }
            Swerve(swerveAmount);
        }

    }

    protected void Swerve(float swerveAmount)
    {
        if(Mathf.Approximately(swerveAmount, 0)) return;
        if((swerveAmount > 0 && !CanMoveRight) || (swerveAmount < 0 && !CanMoveLeft)) return;

        rb.AddRelativeForce(swerveAmount * 10, 0, 0, ForceMode.Impulse);
    }

    protected virtual void StopPlayer()
    {
        rb.velocity = Vector3.zero;
    }

    void StopPlayerX()
    {
        if(MovementType == MovementType.ForwardMovement)
        {
            var vel = rb.velocity;
            vel.x = 0;
            rb.velocity = vel;
        }
        else if(MovementType == MovementType.PathMovement)
        {
            rb.velocity = Vector3.zero;
        }
        else
        {
            Debug.Log("Movement type is not defined");
        }
    }

    public void SetSwerveParameters(float swerveSpeed, float swerveAmount)
    {
        this.swerveSpeed = swerveSpeed;
        this.maxSwerveAmount = swerveAmount;
    }

    public void ToggleSwerve(bool value)
    {
        swerveEnabled = value;
    }
}
