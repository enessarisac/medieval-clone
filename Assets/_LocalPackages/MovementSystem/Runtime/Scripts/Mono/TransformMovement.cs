using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;

public class TransformMovement : MonoBehaviour
{
    protected virtual void Awake() {
        
    }

    /// <summary> Move transform to target position </summary>
    [Button("Move to Destionation")]
    public virtual void MoveToDestination(Vector3 position,float duration)
    {
        Vector3 targetPos = position;
        Vector3 desiredForward = (targetPos - this.transform.position).normalized;
        Quaternion desiredRotation = Quaternion.LookRotation(desiredForward);
        var rotationVector = desiredRotation.eulerAngles;
        rotationVector.y -= 180;
        // this.transform.DORotate(rotationVector, 0.5f, RotateMode.Fast);
        this.transform.DOMove(targetPos, duration).SetEase(Ease.Linear).OnComplete(OnMoveCompleted);
    }

    /// <summary> Move transform to position of the target transform </summary>
    public virtual void MoveToDestination(Transform transform,float duration)
    {
        Vector3 targetPos = transform.position;
        Vector3 desiredForward = (transform.position - this.transform.position).normalized;
        Quaternion desiredRotation = Quaternion.LookRotation(desiredForward);
        var rotationVector = desiredRotation.eulerAngles;
        rotationVector.y += 180;
        RotateToDestination(rotationVector);
        this.transform.DOMove(targetPos, duration).SetEase(Ease.Linear).OnComplete(OnMoveCompleted);
    }

    protected virtual void RotateToDestination(Vector3 rotationVector)
    {
        //this.transform.DORotate(rotationVector, 0.5f, RotateMode.Fast);
    }

    /// <summary> Move transform to position of the target transform. Can be overridden to add climb animation. </summary>
    public virtual void ClimbToDestination(Transform transform, float duration)
    {
        Vector3 targetPos = transform.position;
        Vector3 desiredForward = (transform.position - this.transform.position).normalized;
        Quaternion desiredRotation = Quaternion.LookRotation(desiredForward);
        var rotationVector = desiredRotation.eulerAngles;
        rotationVector.y += 180;
        this.transform.DORotate(rotationVector, 0.5f, RotateMode.Fast);
        this.transform.DOMove(targetPos, duration).SetEase(Ease.Linear).OnComplete(OnMoveCompleted);
    }

    protected virtual void OnMoveCompleted()
    {

    }
}
