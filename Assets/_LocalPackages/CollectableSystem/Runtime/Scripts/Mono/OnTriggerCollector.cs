using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace CollectableSystem
{
    public abstract class OnTriggerCollector : MonoBehaviour, ICanCollect
    {
        [ReadOnly] [SerializeField] List<ICollectable> collected = new List<ICollectable>();
        public List<ICollectable> GetCollected => collected;

        [SerializeField] UnityEvent<ICollectable> onCollect;
        public UnityEvent<ICollectable> OnCollect => onCollect;

        public void Collect(ICollectable ICollectable){}

        protected virtual void OnTrigger(GameObject go)
        {
            var collectable = go.GetComponent<ICollectable>();
            if(!collectable.IsNull())
            {
                collected.Add(collectable);
                OnCollect?.Invoke(collectable);
                collectable.GetCollected();
            }

        }
    }

}