using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace CollectableSystem
{
    public class Collectable : MonoBehaviour, ICollectable
    {
        [SerializeField] UnityEvent onCollected;
        public UnityEvent OnCollected => onCollected;
        [SerializeField] UnityEvent onCollectFailed;
        public UnityEvent OnCollectFailed => onCollectFailed;
        public GameObject CollectObject
        {
            get { return gameObject; }
        }
        public bool IsCollected { get; set; }
        public int amount { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }


        public void GetCollected()
        {
            Debug.Log("Collectable collected");
            IsCollected = true;
            gameObject.SetActive(false);
        }
    }
}

