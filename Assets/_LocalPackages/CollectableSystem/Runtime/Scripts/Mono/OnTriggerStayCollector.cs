﻿using UnityEngine;

namespace CollectableSystem
{
    public class OnTriggerStayCollector : OnTriggerCollector
    {
        float cooldownTime = 1;
        Cooldown cooldown;

        private void Awake()
        {
            cooldown = new Cooldown(cooldownTime);
        }

        private void OnTriggerStay(Collider other)
        {
            if (cooldown.IsCooldownFinished)
            {
                OnTrigger(other.gameObject);
            }
        }

        protected override void OnTrigger(GameObject go)
        {
            var collectable = go.GetComponent<ICollectable>();
            if(!collectable.IsNull())
            {
                OnCollect?.Invoke(collectable);
                cooldown.UpdateCooldown();
            }
        }
    }

}
