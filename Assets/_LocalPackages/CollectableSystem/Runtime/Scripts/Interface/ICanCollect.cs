﻿using System.Collections.Generic;
using UnityEngine.Events;

namespace CollectableSystem
{
    public interface ICanCollect
    {
        UnityEvent<ICollectable> OnCollect { get; }
        List<ICollectable> GetCollected { get; }
        void Collect(ICollectable ICollectable);
    }

}