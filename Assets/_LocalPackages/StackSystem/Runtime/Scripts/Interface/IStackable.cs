using UnityEngine.Events;

namespace StackSystem
{
    public interface IStackable
    {
        UnityEvent OnStacked{get;}
        UnityEvent OnUnStacked{get;}
        bool IsStacked { get; }
        void GetStacked();
        void GetUnstacked();

    }
}