using ScriptableSystem;
using UnityEngine;
using UnityEngine.Events;

namespace EventsSystem
{


    public class OnTriggerEvent : MonoBehaviour
    {
        //[SerializeField] string triggerTag;
        [SerializeField] StringReference stringReference;

        [SerializeField] UnityEvent onEnter;
        [SerializeField] UnityEvent<GameObject> onEnterGO;
        [SerializeField] bool callOnEnterAtStart;
        [SerializeField] UnityEvent onExit;
        [SerializeField] UnityEvent<GameObject> onExitGO;
        [SerializeField] bool callOnExitAtStart;

        private void Start()
        {
            if (callOnEnterAtStart)
                onEnter?.Invoke();
            if (callOnExitAtStart)
                onExit?.Invoke();
        }

        private void OnTriggerEnter(Collider other)
        {

            if (stringReference.Value == "" || other.CompareTag(stringReference.Value))
            {
                onEnter?.Invoke();
                onEnterGO?.Invoke(other.gameObject);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (stringReference.Value == "" || other.CompareTag(stringReference.Value))
            {
                onExit?.Invoke();
                onExitGO?.Invoke(other.gameObject);
            }
        }
    }

}