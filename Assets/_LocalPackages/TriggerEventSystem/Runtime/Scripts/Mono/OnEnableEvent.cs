﻿using UnityEngine;
using UnityEngine.Events;

namespace EventsSystem
{
    public class OnEnableEvent : MonoBehaviour
    {
        [SerializeField] UnityEvent onEnable;

        private void OnEnable()
        {
            onEnable?.Invoke();
        }
    }
}