using UnityEngine;
using UnityEngine.Events;

namespace DroppableSystem
{
    public class Dropper : MonoBehaviour, ICanDrop
    {
        [SerializeField]UnityEvent<IDroppable> onDropped;
        public UnityEvent<IDroppable> OnDropped => onDropped;

        public void Drop(IDroppable IDroppable)
        {
            OnDropped?.Invoke(IDroppable);
        }
    }
}
