using GameLevelSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace FeedbackSystem
{
    public class FeedbackTextController : MonoBehaviour
    {
        [SerializeField] Transform FeedbackCanvas;
        [SerializeField] GameObject textPrefab;
        float upOffset = 5f;

        public static FeedbackTextController Instance;

        private void Awake()
        {
            Instance = this;
        }

        public void ShowTextFeedback(int value)
        {
            if (value < 0)
                ShowDamageTextRed(value);
            else
                ShowDamageTextGreen(value);
        }

        private FeedbackText CreateText(int damage)
        {
            FeedbackText text = Instantiate(textPrefab, transform.position + transform.up * upOffset, Quaternion.identity).GetComponent<FeedbackText>();
            text.transform.parent = FeedbackCanvas;
            text.SetDamageAmount(damage);
            return text;
        }

        public void ShowDamageTextRed(int damage)
        {
            FeedbackText text = CreateText(damage);
            text.SetColorRed();
        }

        public void ShowDamageTextGreen(int damage)
        {
            FeedbackText text = CreateText(damage);
            text.SetColorGreen();
        }

        public void ShowDamageTextYellow(int damage)
        {
            FeedbackText text = CreateText(damage);
            text.SetColorYellow();
        }

        public void ShowDamageTextWhite(int damage)
        {
            FeedbackText text = CreateText(damage);
            text.SetDamageAmount(damage);
        }

        public void ShowYellowText(int damage)
        {
            FeedbackText text = CreateText(damage);
            text.SetColorYellow();
        }

        [ContextMenu("Test Damage")]
        public void ShowTestDamage()
        {
            ShowDamageTextRed(-20);
        }

    }
}