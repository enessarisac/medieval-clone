﻿using UnityEngine;

namespace SimilaritySystem
{
    [CreateAssetMenu(menuName = "SimilaritySystem/DataPoints")]
    public class DataPoints : ScriptableObject
    {
        public float[] Values;
        public int DataPointCount{get { return Values.Length; } }

        public void UpdateData(DataPoints dataPoints)
        {
            Values = dataPoints.Values;
        }

    }
}
