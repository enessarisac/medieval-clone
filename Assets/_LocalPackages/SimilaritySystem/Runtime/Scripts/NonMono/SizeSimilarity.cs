using System;
using ScriptableSystem;
using Sirenix.OdinInspector;
using UnityEngine;

namespace SimilaritySystem
{

    [Serializable]
    public class SizeSimilarity : Similarity
    {
        public DataPoints CurrentPoints;
        public DataPoints TargetPoints;
        public float InitialValue=1;
        public float MaxValue=1;
        float totalMaxDif { get { return MaxValue * TargetPoints.Values.Length; } }

        public override float GetSimilarityPercentage()
        {
            if(TargetPoints.IsNull())
            {
                Debug.Log("No target points");
                return 0;
            }
            if (CurrentPoints.IsNull())
            {
                Debug.Log("No current points");
                return 0;
            }
            var similarityPoint = totalMaxDif;

            for (int i = 0; i < TargetPoints.DataPointCount; i++)
            {
                var currentDif = i < CurrentPoints.DataPointCount ? Mathf.Abs(TargetPoints.Values[i] - CurrentPoints.Values[i]) : MaxValue;
                similarityPoint -= Mathf.Min(currentDif, MaxValue);
            }

            return Mathf.Ceil(similarityPoint/ totalMaxDif*100);
        }
    }
}
